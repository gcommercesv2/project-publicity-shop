// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("homeCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, API_URL_IMG) {
    $scope.title = "Home";
    auth.isToken();
    auth.session();
    $scope.user = $localStorage.name;
    const uriinfoAvatar = API_URL + "api/infoAvatar/";

    $http.get(uriinfoAvatar).then(function (response) {
        $scope.imgAvatar = API_URL_IMG + response.data.image;
    });
});

app.controller("logoutCrt", function ($scope, $window, $http, API_URL, $localStorage) {

    const uri = API_URL + "api/logout/";

    $http.post(uri, {"token": $localStorage.token}, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .then(function (success) {
            $localStorage.token = success.data.token;
            ;
            $localStorage.email = success.data.email;
            $localStorage.name = success.data.name;
            window.location = "/login";


        }, function (error) {

        });

});
