/**
 * Created by maria garcia on 27-08-2017.
 */
app.controller("reportCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, API_URL_IMG, CRUD) {
    $scope.title = "Reportes";
    auth.isToken();
    auth.session();
    var brw = new Browser();
    //$scope.user = $localStorage.name;
    //const uriinfoAvatar = API_URL + "api/infoAvatar/";
    //
    //$http.get(uriinfoAvatar).then(function (response) {
    //    $scope.imgAvatar = API_URL_IMG + response.data.image;
    //});
    $scope.domain = API_URL_IMG;
    const uriprd = API_URL + "api/product/";
    $scope.dataTableProduct = false;
    $scope.opProduct = 1;
    $scope.viewframe = 0;
    $scope.openProduct = function () {
        $scope.titleReport = "Product";
        $scope.dataTableProduct = true;
    };

    $scope.pdfProduct = function () {
        $('#myModal').modal();
        $scope.viewframe = 1;

        var html2obj = html2canvas(document.getElementById('rptProduct'), {
            useCORS: true,
            logging: true
            //,
            //allowTaint:false
        });

        var queue = html2obj.parse();
        var canvas = html2obj.render(queue);
        var img = canvas.toDataURL();

        var doc = new jsPDF({
            unit: 'px',
            format: 'a4'
        });

        doc.addImage(img, 'PNG', 10, 10);


        var string = doc.output('datauristring');
        //string=string+'?#toolbar=0';
        $('iframe').attr('src', string);
    }

    $scope.rpProduct = function () {
        $http.get(uriprd + CRUD[0] + "/0/").then(function (response) {
            $scope.dataProduct = response.data;

        });
    }

    $scope.rpProduct();


});
//<input type="file" onchange="previewFile()"><br>
//<img src="" height="200" alt="Image preview...">
function previewFile() {
    var preview = document.querySelector('img');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}