app.controller("securityCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Seguridad";

    const urlmiddle = API_URL + "api/middleware/admin/";
    $scope.initEvent();
    $scope.fireEvent();

    $scope.active = "Activo";
    $scope.inactive = "Inactivo";
    $scope.item = [
        {"name": $scope.inactive, "status": 0},
        {"name": $scope.active, "status": 1}
    ];

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });

});

app.controller("listRolesCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Lista de roles";

    const uri = API_URL + "api/roles/";
    const uria = API_URL + "api/url/";
    const urlmiddle = API_URL + "api/middleware/admin/";
    $scope.initEvent();
    $scope.fireEvent();

    $scope.arraycrud = [
        {
            name: "leer",
            id: CRUD[0]
        },
        {
            name: "guardar",
            id: CRUD[1]
        },
        {
            name: "editar",
            id: CRUD[2]
        },
        {
            name: "actualizar",
            id: CRUD[3]
        },
        {
            name: "eliminar",
            id: CRUD[4]
        }
    ];
    $scope.items = [];
    $scope.add = function () {
        try {
            $scope.items.push($scope.val);
        } catch (e) {

        }
    }

    $scope.del = function (i) {
        $scope.items.splice(i, 1);
    }

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
            var dt = response.data;
            var v = dt.readwrite;
            var r, s, e, u, rm;

            r = auth.getRole(v, CRUD[0]);
            $scope.fread = (CRUD[0] == r) ? CRUD[0] : 0;

            s = auth.getRole(v, CRUD[1]);
            $scope.fsave = (CRUD[1] == s) ? CRUD[1] : 0;


            e = auth.getRole(v, CRUD[2]);
            $scope.fedit = (CRUD[2] == e) ? CRUD[2] : 0;

            u = auth.getRole(v, CRUD[3]);
            $scope.fupdate = (CRUD[3] == u) ? CRUD[3] : 0;

            rm = auth.getRole(v, CRUD[4]);
            $scope.fremove = (CRUD[4] == rm) ? CRUD[4] : 0;

        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });


    $scope.save = function () {
        var c = {"readwrite": $scope.items};
        angular.merge($scope.roles, c);
        $http.post(uri + CRUD[1] + "/0/", $scope.roles, {headers: {'Content-Type': contentType}})
            .then(function () {
                location.reload();
            });
    };
    $scope.edit = function (id) {

        $scope.initEditEvent();
        $http.get(uri + CRUD[2] + "/" + id + "/")
            .then(function (xhr) {
                $scope.postEditEvent();
                if (xhr.data.readwrite.length != undefined || xhr.data.readwrite.length != 0) {
                    $scope.items = xhr.data.readwrite;
                }
                $scope.roles = xhr.data;

                $scope.name = xhr.data._id;
            });
    };
    $scope.upd = function (name) {
        $scope.name = name;
        var c = {"readwrite": $scope.items};
        angular.merge($scope.roles, c);
        $http.post(uri + CRUD[3] + "/" + $scope.name + "/", $scope.roles, {headers: {'Content-Type': contentType}})
            .then(function () {
                $scope.roles = {};
                location.reload();
            });
    };

    $scope.confirm = function (id, dni) {
        $scope.deleteRecord = "Eliminar registro";
        $scope.sureDelete = "Esta seguro de eliminar";
        $scope.ofRecord = "del registro";
        $scope.yesSure = "Si";
        $scope.not = "No";
        $.confirm({
            title: $scope.deleteRecord + "",
            text: $scope.sureDelete + ' "' + dni + '" ' + $scope.ofRecord + '?',
            confirm: function () {

                $http.get(uri + CRUD[4] + "/" + id + "/")
                    .then(function () {
                        location.reload();
                    });
            },
            cancel: function () {
                // cancel
            },
            confirmButton: $scope.yesSure,
            cancelButton: $scope.not
        });
    };

    $http.get(uri + CRUD[0] + "/0/").then(function (response) {
        $scope.data = response.data;
    });

    $http.get(uria + CRUD[0] + "/0/").then(function (response) {
        $scope.urlapi = response.data;
    });


});

app.controller("listUserCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Lista de usuarios";
    const uri = API_URL + "api/user/";
    const urir = API_URL + "api/roles/";
    const urlmiddle = API_URL + "api/middleware/admin/";
    $scope.initEvent();
    $scope.fireEvent();

    $scope.items = [];
    $scope.items1 = [];
    $scope.val = {};
    $scope.add = function () {
        console.log("...." + JSON.stringify($scope.val));
        var ls = JSON.parse($scope.val);
        try {
            var vl = {"_id": ls._id, "name": ls.name, "readwrite": ls.readwrite};
            $scope.items.push(vl);
            $scope.items1.push({"_id": ls._id});
        } catch (e) {

        }
    }

    $scope.del = function (i) {
        $scope.items.splice(i, 1);
        $scope.items1.splice(i, 1);
        //$scope.items1=$scope.items;
    }

    $scope.isResetPass = function () {
        var c = $scope.user.isResetPassword;

        //console.log("...,,, " + c);
        if (c == true) {
            $scope.rst = 1;
            $scope.user.password = null;
        } else {
            $scope.rst = undefined;
        }
    }

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
            var dt = response.data;
            var v = dt.readwrite;
            var r, s, e, u, rm;

            r = auth.getRole(v, CRUD[0]);
            $scope.fread = (CRUD[0] == r) ? CRUD[0] : 0;

            s = auth.getRole(v, CRUD[1]);
            $scope.fsave = (CRUD[1] == s) ? CRUD[1] : 0;


            e = auth.getRole(v, CRUD[2]);
            $scope.fedit = (CRUD[2] == e) ? CRUD[2] : 0;

            u = auth.getRole(v, CRUD[3]);
            $scope.fupdate = (CRUD[3] == u) ? CRUD[3] : 0;

            rm = auth.getRole(v, CRUD[4]);
            $scope.fremove = (CRUD[4] == rm) ? CRUD[4] : 0;

        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });


    $scope.save = function () {
        // var vls = {"roles": $scope.items, "rolID": $scope.items1};
        var vls = {"roles": $scope.items, "rolID": $scope.items1};
        angular.merge($scope.user, vls);
        $http.post(uri + CRUD[1] + "/0/", $scope.user, {headers: {'Content-Type': contentType}})
            .then(function () {
                location.reload();
            });
    };
    $scope.edit = function (id) {

        $scope.initEditEvent();
        $http.get(uri + CRUD[2] + "/" + id + "/")
            .then(function (xhr) {
                $scope.postEditEvent();
                if (xhr.data.roles.length != undefined || xhr.data.roles.length != 0) {
                    $scope.items = xhr.data.roles;
                    $scope.items1 = xhr.data.rolID;
                }

                $scope.user = xhr.data;
                $scope.name = xhr.data._id;
                //$scope.items = xhr.data.roles;
            });
    };
    $scope.upd = function (name) {
        $scope.name = name;
        //var vls = {"roles": $scope.items};
        var vls = {"roles": $scope.items, "rolID": $scope.items1};
        angular.merge($scope.user, vls);
        $http.post(uri + CRUD[3] + "/" + $scope.name + "/", $scope.user, {headers: {'Content-Type': contentType}})
            .then(function () {
                $scope.user = {};
                location.reload();
            });
    };

    $scope.confirm = function (id, dni) {
        $scope.deleteRecord = "Eliminar registro";
        $scope.sureDelete = "Esta seguro de eliminar";
        $scope.ofRecord = "del registro";
        $scope.yesSure = "Si";
        $scope.not = "No";
        $.confirm({
            title: $scope.deleteRecord + "",
            text: $scope.sureDelete + ' "' + dni + '" ' + $scope.ofRecord + '?',
            confirm: function () {

                $http.get(uri + CRUD[4] + "/" + id + "/")
                    .then(function () {
                        location.reload();
                    });
            },
            cancel: function () {
                // cancel
            },
            confirmButton: $scope.yesSure,
            cancelButton: $scope.not
        });
    };

    $http.get(uri + CRUD[0] + "/0/").then(function (response) {
        $scope.data = response.data;
    });

    $http.get(urir + CRUD[0] + "/0/").then(function (response) {
        $scope.roles = response.data;
    });


});

app.controller("settingsCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, API_URL_IMG, contentType) {

    //------
    // $scope.initEvent = function () {
    //     $scope.opFrm = 0;
    //     $scope.opBtn = 1;
    //     $scope.clFrm = 0;
    //     $scope.clBtn = 0;
    //     $scope.dataTable = true;
    // }
    //
    // $scope.fireEvent = function () {
    //     $scope.openFrm = function () {
    //         $scope.opFrm = 1;
    //         $scope.opBtn = 0;
    //         $scope.clFrm = 0;
    //         $scope.clBtn = 1;
    //         $scope.opFrm2 = false;
    //         $scope.dataTable = false;
    //     };
    //     $scope.closeFrm = function () {
    //         $scope.opFrm = 0;
    //         $scope.opBtn = 1;
    //         $scope.clFrm = 1;
    //         $scope.clBtn = 0;
    //         $scope.opFrm2 = false;
    //         $scope.dataTable = true;
    //         location.reload();
    //     };
    // }
    //
    // $scope.initEditEvent = function () {
    //     $scope.opFrm = 0;
    //     $scope.clBtn = 1;
    // }
    //
    // $scope.postEditEvent = function () {
    //     $scope.opBtn = 0;
    //     $scope.opFrm2 = true;
    //     $scope.dataTable = false;
    // }
    $scope.h = 1;
    $scope.m = 0;
    $scope.a = 0;
    $scope.home = function () {
        $scope.h = 1;
        $scope.m = 0;
        $scope.a = 0;
    }
    $scope.menu = function () {
        $scope.h = 0;
        $scope.m = 1;
        $scope.a = 0;
    }

    $scope.avatar = function () {
        $scope.h = 0;
        $scope.m = 0;
        $scope.a = 1;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Configuracion";

    const urlmiddle = API_URL + "api/middleware/user/";
    const uri = API_URL + "api/resetPassword/";
    const uriavatar = API_URL + "api/addAvatar/";
    const uriinfo = API_URL + "api/info/";
    const uriinfoAvatar = API_URL + "api/infoAvatar/";
    // $scope.initEvent();
    // $scope.fireEvent();
    // $scope.active = "Activo";
    // $scope.inactive = "Inactivo";
    // $scope.item = [
    //     {"name": $scope.inactive, "status": 0},
    //     {"name": $scope.active, "status": 1}
    // ];

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });

    $scope.changepass = function () {
        //console.log("-----/// " + $scope.password);
        var payload = {"email": $localStorage.email, "password": $scope.password};
        $scope.user = payload;
        $http.post(uri, $scope.user, {headers: {'Content-Type': contentType}})
            .then(function () {
                location.reload();
            });
    }

    $http.get(uriinfo).then(function (response) {
        $scope.info = response.data;
    });

    $http.get(uriinfoAvatar).then(function (response) {
        $scope.imgAvatar = API_URL_IMG + response.data.image;
    });

    $scope.save = function () {
        var file;
        if ($scope.file == undefined) {
            file = $scope.user.image;
        } else {
            file = $scope.file;
        }

        var urlf = uriavatar;
        var formData = new FormData();

        formData.append("email", $localStorage.email);
        formData.append("avatar", file);
        $http.post(urlf, formData, {
            headers: {
                "Content-type": undefined
            },
            transformRequest: angular.identity
        }).then(function (xhr) {
            location.reload();
        });
    }

});