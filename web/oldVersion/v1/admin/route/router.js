/**
 * Created by maria garcia on 07-05-2017.
 */
app.config(function ($routeProvider) {

    $routeProvider
        //.when('/login', {
        //    templateUrl: 'template/login/login.html',
        //    controller: 'loginCrt'
        ////})
        .when('/', {
            templateUrl: 'template/home/home.html',
            controller: 'homeCrt'
        })
        .when('/logout', {
            templateUrl: 'template/exit.html',
            controller: 'logoutCrt'
        })
        .when('/security', {
            templateUrl: 'template/security/security.html',
            controller: 'securityCrt'
        })
        .when('/settings', {
            templateUrl: 'template/security/settings.html',
            controller: 'settingsCrt'
        })
        .when('/list-user', {
            templateUrl: 'template/security/list-user.html',
            controller: 'listUserCrt'
        })
        .when('/list-roles', {
            templateUrl: 'template/security/list-roles.html',
            controller: 'listRolesCrt'
        })
        .when('/shop', {
            templateUrl: 'template/product/shop.html',
            controller: 'shopCrt'
        })
        .when('/coins', {
            templateUrl: 'template/account/coins.html',
            controller: 'coinsCrt'
        })
        .when('/status', {
            templateUrl: 'template/account/status.html',
            controller: 'statusCrt'
        })
        .when('/tax', {
            templateUrl: 'template/account/tax.html',
            controller: 'taxCrt'
        })
        .when('/report', {
            templateUrl: 'template/report/report.html',
            controller: 'reportCrt'
        })
        .when('/category', {
            templateUrl: 'template/product/category.html',
            controller: 'categoryCrt'
        })
        .when('/product', {
            templateUrl: 'template/product/product.html',
            controller: 'productCrt'
        })
        .otherwise({
            redirectTo: '/'
        });
});
