app.controller("utilityCrt", function ($scope) {
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
});