var mongoose = require('mongoose');
var User = require('../model/Oauth');
var TwitterStrategy = require('passport-twitter').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var config = require('../config/OauthConfig');
module.exports = function (passport) {

    passport.serializeUser(function (user, done) {
        done(null, user);
        //done(null, user.id);
    });

    passport.deserializeUser(function (obj, done) {

        done(null, obj);
        // User.findById(id, function(err, user) {
        //           done(err, user);
        //       });
    });

    passport.use(new TwitterStrategy({
        consumerKey: 'TWITTER_CONSUMER_KEY',
        consumerSecret: 'TWITTER_CONSUMER_SECRET',
        callbackURL: '/auth/twitter/callback'
    }, function (accessToken, refreshToken, profile, done) {

        User.findOne({provider_id: profile.id}, function (err, user) {
            if (err) throw(err);
            if (!err && user != null) return done(null, user);

            var user = new User({
                provider_id: profile.id,
                provider: profile.provider,
                name: profile.displayName,
                photo: profile.photos[0].value
            });
            user.save(function (err) {
                if (err) throw err;
                done(null, user);
            });
        });
    }));
    passport.use(new FacebookStrategy({
        clientID: config.facebook.id,
        clientSecret: config.facebook.secret,
        callbackURL: '/api/oauth/facebook/callback'
    }, function (accessToken, refreshToken, profile, done) {
        console.log("-------// " + accessToken + " /// " + refreshToken + " /// " + profile);
        User.findOne({provider_id: profile.id}, function (err, user) {
            if (err) throw(err);
            if (!err && user != null) return done(null, user);

            var user = new User({
                provider_id: profile.id,
                provider: profile.provider,
                name: profile.displayName,
                photo: profile.photos[0].value
            });
            user.save(function (err) {
                if (err) throw err;
                done(null, user);
            });
        });
    }));
    passport.use(new GoogleStrategy({
        clientID: config.google.id,
        clientSecret: config.google.secret,
        callbackURL: 'http://localhost:8090/api/auth/google/callback'
    }, function (accessToken, refreshToken, profile, done) {
        process.nextTick(function () {

            //console.log("....profile++++ "+JSON.stringify(profile));

            User.findOne({provider_id: profile.id}, function (err, usr) {

                if (err) return done(err);

                if (usr) {

                    // if a user is found, log them in
                    return done(null, usr);
                } else {
                    var users = new User({
                        provider_id: profile.id,
                        provider: profile.provider,
                        name: profile.displayName,
                        email: profile.emails[0].value,
                        photo: profile.photos[0].value
                    });
                    users.save(function (err) {
                        if (err) throw err;
                        done(null, users);
                    });

                }

            });

        });

    }));


}

