// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("homeCrt", function ($scope, $window, $http, API_URL, API_URL_LOCAL, $localStorage, auth, API_URL_IMG) {
    $scope.title = "Home";
    //console.log(".... " + $localStorage.oauth2 + " ..name   " + $localStorage.name);
    var message="Validando";
    $scope.sessionX = function () {
        if ($localStorage.oauth2 == true) {
            auth.isToken();
            if ($localStorage.email == undefined) {
                $scope.user = message; 
                auth.refreshOauth();
            }
            $scope.user = $localStorage.name;
            auth.sessionOauth();
            $scope.imgAvatar = $localStorage.photo;
        } else if ($localStorage.oauth2 == false) {
            auth.isToken();
            auth.session();
            $scope.user = $localStorage.name;
            const uriinfoAvatar = API_URL + "api/infoAvatar/";

            $http.get(uriinfoAvatar).then(function (response) {
                $scope.imgAvatar = API_URL_IMG + response.data.image;
            });
        }
    }
    $scope.sessionX();

    $scope.user;

});

app.controller("logoutCrt", function ($scope, $window, $http, API_URL, API_URL_LOCAL, $localStorage) {

    const uri = API_URL + "api/logout/";
    const uriinfoAvatar = API_URL_LOCAL + "api/auth/logout";

    if ($localStorage.oauth2 == false) {
        $http.post(uri, {"token": $localStorage.token}, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(function (success) {
                $localStorage.token = success.data.token;
                ;
                $localStorage.email = success.data.email;
                $localStorage.name = success.data.name;
                window.location = "/signin";


            }, function (error) {

            });
    }

    if ($localStorage.oauth2 == true) {

        $http.get(uriinfoAvatar).then(function (success) {
            $localStorage.name = undefined;
            $localStorage.email = undefined;
            $localStorage.photo = undefined;
            $localStorage.token = undefined;
            $localStorage.oauth2 = undefined;
            window.location = "/signin";
        });
    }

});

// app.controller("loadCrt", function ($scope, $window, $http, API_URL, API_URL_LOCAL, $localStorage, auth, API_URL_IMG) {
//   console.log("....oauth2 " + $localStorage.oauth2 + " ..name   " + $localStorage.name);
//   if ($localStorage.oauth2 == true) {
//     if ($localStorage.email == undefined) {
//         auth.asyncAuthSession().then(function(xhr){
//           console.log(" .... xhr  "+JSON.stringify(xhr.data));
//           $localStorage.token=xhr.data.token;
//           $http.defaults.xsrfCookieName = 'Authorization';
//           $http.defaults.xsrfHeaderName = 'Authorization';
//           $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
//           //window.location="/site";

//         });
//         //auth.sessionOauth();
//         //auth.sessionOauthBearer();
//         // window.location="/site";
//       }
//       auth.asyncAuthSession().then(function(xhr){
//           console.log(" ....1 xhr  "+JSON.stringify(xhr.data));
//           $localStorage.token=xhr.data.token;
//           $http.defaults.xsrfCookieName = 'Authorization';
//           $http.defaults.xsrfHeaderName = 'Authorization';
//           $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;

//         });
//   }else {
//     auth.isToken();
//     auth.session();
//     window.location="/site";
//   }

// });