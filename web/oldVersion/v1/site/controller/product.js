app.controller("productInfoCrt", function ($scope, $window, $http, API_URL, $localStorage, $q, auth, API_URL_IMG, contentType) {
    $scope.title = "Productos";
    auth.isToken();
    if ($localStorage.oauth2 == false) {
        auth.session();
    } else {
        auth.sessionOauth();
    }

    function objectFindByKey(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
        return null;
    }

    $scope.domain = API_URL_IMG;
    const uriprd = API_URL + "api/productInfo/";
    const uriprdt = API_URL + "api/productInfoDetail/";
    const urienc = API_URL + "api/chiper-payload/encode";
    const uripay = API_URL + "api/validPaymentProduct";
    const uripayOne = API_URL + "api/validPaymentProductOne";
    //const uridec = API_URL + "api/chiper-payload/decode";
    $scope.dataTableProduct = true;
    $scope.items = [];
    $scope.cant = 0;
    $scope.cants = 0;
    $localStorage.shopping;
    $scope.add = function (id, details, cant) {

        try {

            var val = details._id;
            var vl;
            var payload = {"payload": details};
            var pay;

            if (cant != 0) {
                $scope.cant = cant;
            }

            if ($scope.cant != 0) {

                var result_obj = objectFindByKey($scope.items, '_id', val);
                var obj = JSON.parse(result_obj);
                if (obj == null) {

                    $http.post(urienc, payload, {headers: {'Content-Type': contentType}})
                        .then(function (xhr) {
                            pay = xhr.data;
                            $http.post(uripayOne, pay, {headers: {'Content-Type': contentType}}).then(function (xhr) {
                                var dt = xhr.data.payload;
                                var price = dt.amount.price.toFixed(2);
                                $scope.message = xhr.data.message;
                                vl = {
                                    "_id": id,
                                    "cant": $scope.cant,
                                    "name": dt.name,
                                    "coins":dt.coinsID.name,
                                    "tax": dt.taxID.value,
                                    "tax_name": dt.taxID.name,
                                    "price": price
                                };

                                $scope.items.push(vl);

                                $scope.cant = 0;

                                $("[data-dismiss=modal]").trigger({type: "click"});

                                $scope.numberItem();

                            }, function (err) {
                                $scope.message = err.message;
                            });
                        });
                }
            }
        } catch (e) {

        }
    }

    $scope.countItem = 0;
    $scope.numberItem = function () {
        var sum = 0;
        $scope.items.forEach(function (i) {
            sum = sum + i.cant;
        });
        $scope.countItem = sum;
    }

    $scope.resultPayment = function () {

        $('#myModal1').modal();
        $scope.statusCart(0, 0);
    }

    $scope.del = function (i) {

        $scope.items.splice(i, 1);
        $scope.statusCart(0, 0);

    }

    $scope.truncate = function () {
        $scope.items = [];
    }

    $scope.payment = function () {
        var payload = {"payload": $scope.items};
        var pay;
        $http.post(urienc, payload, {headers: {'Content-Type': contentType}}).then(function (xhr) {
            pay = xhr.data;
            $http.post(uripay, pay, {headers: {'Content-Type': contentType}}).then(function (xhr) {
                location.reload();
            }, function (err) {

            })
        }, function (err) {

        });
    }

    $scope.statusCart = function (id, cant) {
        var totals = 0;
        $scope.total = 0;
        var sums = 0;
        var tax = 0;
        var price = 0;
        var count = 0;
        $scope.items.forEach(function (i) {


            if (i._id == id) {
                i.cant = cant;
            }
            sums = i.cant;
            count = count + sums;

            tax = (parseFloat(i.price)) * (parseFloat(i.tax));
            price = (parseFloat(i.price)) + tax;
            price = price * (parseFloat(sums));

            totals = parseFloat(totals) + price;
            $scope.total = totals.toFixed(2);
        });

        if ($scope.total == 0) {
            $scope.countItem = 0;
        }
        $scope.countItem = count;
    }

    $scope.detailProduct = function (id) {
        $('#myModal').modal();
        var detail = id;
        $http.get(uriprdt + detail).then(function (response) {
            $scope.details = response.data;

        });
    }

    $scope.rpProduct = function () {
        $http.get(uriprd).then(function (response) {
            $scope.dataProduct = response.data;

        });
    }

    $scope.rpProduct();


});
