var app = angular.module("app", ['ngRoute', 'ngStorage', 'ngSanitize', 'colorpicker.module', 'wysiwyg.module']
    , function () {


    }
);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

app.constant("API_URL", "http://localhost:8070/");
app.constant("API_URL_IMG", "http://localhost:8070");
app.constant("API_URL_LOCAL","http://localhost:8090/");
const crud = ["read", "save", "edit", "update", "delete"];
var contentType='application/json';
//var contentType='application/x-www-form-urlencoded';
app.constant("CRUD", crud);
app.constant("contentType", contentType);

app.directive("match", function () {
    return {
        restrict: "A",
        require: "ngModel",

        link: function (scope, element, attrs, ctrl) {

            function matchValidator(value) {

                scope.$watch(attrs.match, function (newValue, oldValue) {

                    var isValid = value === scope.$eval(attrs.match);
                    ctrl.$setValidity('match', isValid);

                });

                return value;
            }

            ctrl.$parsers.push(matchValidator);
        }
    };
});


app.directive('uploaderModel', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            iElement.on("change", function (e) {
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            });
        }
    };
}]);

app.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});

app.directive('errBox', function () {
    return {
        restrict: 'EA',
        scope: {
            fail: '@',
            mess: '@'
        },
        template: '<div ng-if="fail==1"><div class="alert alert-danger">' +
        '<div class="container-fluid"><div class="alert-icon">' +
        '<i class="material-icons">error_outline</i></div>' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
        '<span aria-hidden="true"><i class="material-icons">clear</i></span>' +
        '</button><b>Error:</b> {{mess}}</div></div></div>'
    }
});

app.directive('erBox', function () {
    return {
        restrict: 'E',
        scope: {
            fail: '@',
            mess: '@'
        },
        template: '<div class="alert alert-danger alert-dismissable fade in" ng-if="fail==1">' +
        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
        '<strong>Error!</strong> {{mess}} .' +
        '</div>'
    }
});

