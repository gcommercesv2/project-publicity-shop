app.controller("geoCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, API_URL_IMG) {
    $scope.title = "Geolocalizacion";
    auth.isToken();
    auth.session();
    // $scope.user = $localStorage.name;
    // const uriinfoAvatar = API_URL + "api/infoAvatar/";

    // $http.get(uriinfoAvatar).then(function (response) {
    //     $scope.imgAvatar = API_URL_IMG + response.data.image;
    // });

});

// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("locationCrt", function ($scope, $window, $http, API_URL, API_URL_LOCAL, $localStorage, auth, CRUD, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Ubicacion";


    const uri = API_URL + "api/location/";
    const urlmiddle = API_URL + "api/middleware/admin/";
    $scope.initEvent();
    $scope.fireEvent();

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
            var dt = response.data;
            var v = dt.readwrite;
            var r, s, e, u, rm;

            r = auth.getRole(v, CRUD[0]);
            $scope.fread = (CRUD[0] == r) ? CRUD[0] : 0;

            s = auth.getRole(v, CRUD[1]);
            $scope.fsave = (CRUD[1] == s) ? CRUD[1] : 0;


            e = auth.getRole(v, CRUD[2]);
            $scope.fedit = (CRUD[2] == e) ? CRUD[2] : 0;

            u = auth.getRole(v, CRUD[3]);
            $scope.fupdate = (CRUD[3] == u) ? CRUD[3] : 0;

            rm = auth.getRole(v, CRUD[4]);
            $scope.fremove = (CRUD[4] == rm) ? CRUD[4] : 0;


        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });


    $scope.save = function () {

        $http.post(uri + CRUD[1] + "/0/", $scope.model, {headers: {'Content-Type': contentType}})
            .then(function () {
                location.reload();
            });
    };
    $scope.edit = function (id) {

        $scope.initEditEvent();
        $http.get(uri + CRUD[2] + "/" + id + "/")
            .then(function (xhr) {
                $scope.postEditEvent();

                $scope.model = xhr.data;
                $scope.name = xhr.data._id;

                $scope.mapa = API_URL_LOCAL + "map1/" + xhr.data.name + "/";
            });
    };
    $scope.upd = function (name) {
        $scope.name = name;

        $http.post(uri + CRUD[3] + "/" + $scope.name + "/", $scope.model, {headers: {'Content-Type': contentType}})
            .then(function () {
                $scope.shop = {};
                location.reload();
            });
    };

    $scope.confirm = function (id, dni) {
        $scope.deleteRecord = "Eliminar registro";
        $scope.sureDelete = "Esta seguro de eliminar";
        $scope.ofRecord = "del registro";
        $scope.yesSure = "Si";
        $scope.not = "No";
        $.confirm({
            title: $scope.deleteRecord + "",
            text: $scope.sureDelete + ' "' + dni + '" ' + $scope.ofRecord + '?',
            confirm: function () {

                $http.get(uri + CRUD[4] + "/" + id + "/")
                    .then(function () {
                        location.reload();
                    });
            },
            cancel: function () {
                // cancel
            },
            confirmButton: $scope.yesSure,
            cancelButton: $scope.not
        });
    };

    $http.get(uri + CRUD[0] + "/0/").then(function (response) {
        $scope.data = response.data;
    });

    /* $http.get(uris + CRUD[0] + "/0/").then(function (response) {
     $scope.shops = response.data;
     });

     $http.get(uric + CRUD[0] + "/0/").then(function (response) {
     $scope.categorys = response.data;
     });*/

});

// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("shopCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Tiendas";

    const uri = API_URL + "api/shop/";
    const urlmiddle = API_URL + "api/middleware/shop/";
    const urlLocation= API_URL+"api/location/";
    $scope.initEvent();
    $scope.fireEvent();

    $scope.active = "Activo";
    $scope.inactive = "Inactivo";
    $scope.item = [
        {"name": $scope.inactive, "status": 0},
        {"name": $scope.active, "status": 1}
    ];
    // var contentType='application/json';
    //var contentType='application/x-www-form-urlencoded';

    $scope.selects=function(){
         $('#select').select2();
         $('#selectt').select2();
         $http.get(urlLocation + CRUD[0] + "/0/").then(function(xhr){
            $scope.locationSelect=xhr.data;
         })
    }

    $scope.selects();

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
            var dt = response.data;
            var v = dt.readwrite;
            var r, s, e, u, rm;

            r = auth.getRole(v, CRUD[0]);
            $scope.fread = (CRUD[0] == r) ? CRUD[0] : 0;

            s = auth.getRole(v, CRUD[1]);
            $scope.fsave = (CRUD[1] == s) ? CRUD[1] : 0;


            e = auth.getRole(v, CRUD[2]);
            $scope.fedit = (CRUD[2] == e) ? CRUD[2] : 0;

            u = auth.getRole(v, CRUD[3]);
            $scope.fupdate = (CRUD[3] == u) ? CRUD[3] : 0;

            rm = auth.getRole(v, CRUD[4]);
            $scope.fremove = (CRUD[4] == rm) ? CRUD[4] : 0;


        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });


    $scope.save = function () {
        // var lat = $scope.shop.geolocation.lat;
        // lat = parseFloat(lat);
        // var long = $scope.shop.geolocation.long;
        // long = parseFloat(long)
        // var geo = {"lat": lat, "long": long};
        // var geol = {"geolocation": geo};
        // angular.merge($scope.shop, geol);
        $http.post(uri + CRUD[1] + "/0/", $scope.shop, {headers: {'Content-Type': contentType}})
            .then(function () {
                location.reload();
            });
    };
    $scope.edit = function (id) {

        $scope.initEditEvent();
        $http.get(uri + CRUD[2] + "/" + id + "/")
            .then(function (xhr) {
                $scope.postEditEvent();

                $scope.shop = xhr.data;
                $scope.name = xhr.data._id;
                $scope.shop.locationID=xhr.data.locationID._id;
            });
    };
    $scope.upd = function (name) {
        $scope.name = name;
        // var lat = $scope.shop.geolocation.lat;
        // lat = parseFloat(lat);
        // var long = $scope.shop.geolocation.long;
        // long = parseFloat(long)
        // var geo = {"lat": lat, "long": long};
        // var geol = {"geolocation": geo};
        // angular.merge($scope.shop, geol);
        $http.post(uri + CRUD[3] + "/" + $scope.name + "/", $scope.shop, {headers: {'Content-Type': contentType}})
            .then(function () {
                $scope.shop = {};
                location.reload();
            });
    };

    $scope.confirm = function (id, dni) {
        $scope.deleteRecord = "Eliminar registro";
        $scope.sureDelete = "Esta seguro de eliminar";
        $scope.ofRecord = "del registro";
        $scope.yesSure = "Si";
        $scope.not = "No";
        $.confirm({
            title: $scope.deleteRecord + "",
            text: $scope.sureDelete + ' "' + dni + '" ' + $scope.ofRecord + '?',
            confirm: function () {

                $http.get(uri + CRUD[4] + "/" + id + "/")
                    .then(function () {
                        location.reload();
                    });
            },
            cancel: function () {
                // cancel
            },
            confirmButton: $scope.yesSure,
            cancelButton: $scope.not
        });
    };

    $http.get(uri + CRUD[0] + "/0/").then(function (response) {
        $scope.data = response.data;
    });

    /* $http.get(uris + CRUD[0] + "/0/").then(function (response) {
     $scope.shops = response.data;
     });

     $http.get(uric + CRUD[0] + "/0/").then(function (response) {
     $scope.categorys = response.data;
     });*/

});
