app.controller("loginCrt", function ($scope, $window, $http, API_URL, $localStorage, auth) {
    $scope.title = "Login";
    const uri = API_URL + "api/login/";
    // if ($localStorage.token != undefined) {
    //     window.location = "/";
    // }
    auth.isLoggedUser();
    $scope.signUp = function () {
        var contentType='application/json';
        //var contentType='application/x-www-form-urlencoded';
        $scope.fail = 0; 
        $http.post(uri, $scope.User, {headers: {'Content-Type': contentType}})
            .then(function (success) {
                $scope.token = success.data.token;
                if ($scope.token != undefined) {
                    auth.storage($scope.token, success.data.email, success.data.name,success.data.id);
                    //window.location = "/";
                    window.location = "/admin";

                } else {
                    //tks.storage(undefined,undefined,undefined);
                    //window.location = "http://localhost:9000/login";
                    //$scope.auth = $scope.failedCredentials;
                    $scope.load = 1;
                    $scope.loader = 1;

                    $scope.fail = 1;
                    $scope.mess = "Failed credential";
                }
            }, function (error) {
                $scope.fail = 1;
                $scope.mess = error.data + " Failed credential";
            });
    }


});

app.controller("registerCrt", function ($scope, $window, $http, API_URL, $localStorage) {
    $scope.title = "Login";
    const uri = API_URL + "api/register/";
    var contentType='application/json';
    //var contentType='application/x-www-form-urlencoded';
    $scope.register = function () {
        $http.post(uri, $scope.User, {headers: {'Content-Type': contentType}})
            .then(function (success) {
                window.location = "/login";
            }, function (error) {
            });
    }

});