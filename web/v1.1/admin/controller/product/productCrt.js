// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("productCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, API_URL_IMG) {
    $scope.title = "Producto";
    auth.isToken();
    auth.session();
    // $scope.user = $localStorage.name;
    // const uriinfoAvatar = API_URL + "api/infoAvatar/";

    // $http.get(uriinfoAvatar).then(function (response) {
    //     $scope.imgAvatar = API_URL_IMG + response.data.image;
    // });

});

// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("typeDefCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Definicion de tipo";

    const uri = API_URL + "api/typedef/";
    const urlmiddle = API_URL + "api/middleware/product/";
    $scope.initEvent();
    $scope.fireEvent();

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
            var dt = response.data;
            var v = dt.readwrite;
            var r, s, e, u, rm;

            r = auth.getRole(v, CRUD[0]);
            $scope.fread = (CRUD[0] == r) ? CRUD[0] : 0;

            s = auth.getRole(v, CRUD[1]);
            $scope.fsave = (CRUD[1] == s) ? CRUD[1] : 0;


            e = auth.getRole(v, CRUD[2]);
            $scope.fedit = (CRUD[2] == e) ? CRUD[2] : 0;

            u = auth.getRole(v, CRUD[3]);
            $scope.fupdate = (CRUD[3] == u) ? CRUD[3] : 0;

            rm = auth.getRole(v, CRUD[4]);
            $scope.fremove = (CRUD[4] == rm) ? CRUD[4] : 0;

        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });


    $scope.save = function () {

        $http.post(uri + CRUD[1] + "/0/", $scope.td, {headers: {'Content-Type': contentType}})
            .then(function () {
                location.reload();
            });
    };
    $scope.edit = function (id) {

        $scope.initEditEvent();
        $http.get(uri + CRUD[2] + "/" + id + "/")
            .then(function (xhr) {
                $scope.postEditEvent();

                $scope.td = xhr.data;
                $scope.name = xhr.data._id;
            });
    };
    $scope.upd = function (name) {
        $scope.name = name;
        $http.post(uri + CRUD[3] + "/" + $scope.name + "/", $scope.td, {headers: {'Content-Type': contentType}})
            .then(function () {
                $scope.td = {};
                location.reload();
            });
    };

    $scope.confirm = function (id, dni) {
        $scope.deleteRecord = "Eliminar registro";
        $scope.sureDelete = "Esta seguro de eliminar";
        $scope.ofRecord = "del registro";
        $scope.yesSure = "Si";
        $scope.not = "No";
        $.confirm({
            title: $scope.deleteRecord + "",
            text: $scope.sureDelete + ' "' + dni + '" ' + $scope.ofRecord + '?',
            confirm: function () {

                $http.get(uri + CRUD[4] + "/" + id + "/")
                    .then(function () {
                        location.reload();
                    });
            },
            cancel: function () {
                // cancel
            },
            confirmButton: $scope.yesSure,
            cancelButton: $scope.not
        });
    };

    $http.get(uri + CRUD[0] + "/0/").then(function (response) {
        $scope.data = response.data;
    });


});

// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("warehouseCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Almacen";

    const uri = API_URL + "api/warehouse/";
    const urlmiddle = API_URL + "api/middleware/product/";
    $scope.initEvent();
    $scope.fireEvent();

    $http.get(urlmiddle)
        .then(function (response) {
            //console.log("... "+response);
            var dt = response.data;
            var v = dt.readwrite;
            var r, s, e, u, rm;

            r = auth.getRole(v, CRUD[0]);
            $scope.fread = (CRUD[0] == r) ? CRUD[0] : 0;

            s = auth.getRole(v, CRUD[1]);
            $scope.fsave = (CRUD[1] == s) ? CRUD[1] : 0;


            e = auth.getRole(v, CRUD[2]);
            $scope.fedit = (CRUD[2] == e) ? CRUD[2] : 0;

            u = auth.getRole(v, CRUD[3]);
            $scope.fupdate = (CRUD[3] == u) ? CRUD[3] : 0;

            rm = auth.getRole(v, CRUD[4]);
            $scope.fremove = (CRUD[4] == rm) ? CRUD[4] : 0;

        }, function (error) {
            //var er=JSON.stringify(error);
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
            console.log("..._! " + error.data.enabled);
        });


    $scope.save = function () {

        $http.post(uri + CRUD[1] + "/0/", $scope.wh, {headers: {'Content-Type': contentType}})
            .then(function () {
                location.reload();
            });
    };
    $scope.edit = function (id) {

        $scope.initEditEvent();
        $http.get(uri + CRUD[2] + "/" + id + "/")
            .then(function (xhr) {
                $scope.postEditEvent();

                $scope.wh = xhr.data;
                $scope.name = xhr.data._id;
            });
    };
    $scope.upd = function (name) {
        $scope.name = name;
        $http.post(uri + CRUD[3] + "/" + $scope.name + "/", $scope.wh, {headers: {'Content-Type': contentType}})
            .then(function () {
                $scope.wh = {};
                location.reload();
            });
    };

    $scope.confirm = function (id, dni) {
        $scope.deleteRecord = "Eliminar registro";
        $scope.sureDelete = "Esta seguro de eliminar";
        $scope.ofRecord = "del registro";
        $scope.yesSure = "Si";
        $scope.not = "No";
        $.confirm({
            title: $scope.deleteRecord + "",
            text: $scope.sureDelete + ' "' + dni + '" ' + $scope.ofRecord + '?',
            confirm: function () {

                $http.get(uri + CRUD[4] + "/" + id + "/")
                    .then(function () {
                        location.reload();
                    });
            },
            cancel: function () {
                // cancel
            },
            confirmButton: $scope.yesSure,
            cancelButton: $scope.not
        });
    };

    $http.get(uri + CRUD[0] + "/0/").then(function (response) {
        $scope.data = response.data;
    });


});
// la variable app nop puede ser eliminada ya que hace referencia al modulo declarado en module/app
app.controller("productsCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, CRUD, API_URL_IMG, contentType) {

    //------
    $scope.initEvent = function () {
        $scope.opFrm = 0;
        $scope.opBtn = 1;
        $scope.clFrm = 0;
        $scope.clBtn = 0;
        $scope.dataTable = true;
    }

    $scope.fireEvent = function () {
        $scope.openFrm = function () {
            $scope.opFrm = 1;
            $scope.opBtn = 0;
            $scope.clFrm = 0;
            $scope.clBtn = 1;
            $scope.opFrm2 = false;
            $scope.dataTable = false;
        };
        $scope.closeFrm = function () {
            $scope.opFrm = 0;
            $scope.opBtn = 1;
            $scope.clFrm = 1;
            $scope.clBtn = 0;
            $scope.opFrm2 = false;
            $scope.dataTable = true;
            location.reload();
        };
    }

    $scope.initEditEvent = function () {
        $scope.opFrm = 0;
        $scope.clBtn = 1;
    }

    $scope.postEditEvent = function () {
        $scope.opBtn = 0;
        $scope.opFrm2 = true;
        $scope.dataTable = false;
    }
    //------
    auth.isToken();
    auth.session();
    $scope.title = "Productos";

    //const random = "abcdefghijklmnopqrstuvwxyz1234567890";
    //const longcade = 5;

    //var ls = rand_code(random, longcade);
    var ls = auth.getCodigo();
    $scope.code = ls;
    $scope.domain = API_URL_IMG;
    const uri = API_URL + "api/product/";
    const uris = API_URL + "api/shop/";
    const uric = API_URL + "api/category/";
    const uriwh = API_URL + "api/warehouse/";
    const uritd = API_URL + "api/typedef/";
    const urit = API_URL + "api/tax/";
    const urist = API_URL + "api/status/";
    const uricoins = API_URL + "api/coins/";
    const urlmiddle = API_URL + "api/middleware/product/";
    $scope.initEvent();
    $scope.fireEvent();


    $scope.product = {};
    $scope.copycode = function () {
        var c = $scope.copy;

        if (c == true) {
            $scope.product.code = ls;
        } else {
            $scope.product.code = undefined;
        }
    }
    $scope.copycodeisset = function () {
        var c = $scope.copy;

        if (c == true) {
            $scope.product.code = ls;
        } else {
            $scope.product.code = $scope.product.code;
        }
    }


    $http.get(urlmiddle)
        .then(function (response) {
            var dt = response.data;
            var v = dt.readwrite;
            var r, s, e, u, rm;

            r = auth.getRole(v, CRUD[0]);
            $scope.fread = (CRUD[0] == r) ? CRUD[0] : 0;

            s = auth.getRole(v, CRUD[1]);
            $scope.fsave = (CRUD[1] == s) ? CRUD[1] : 0;


            e = auth.getRole(v, CRUD[2]);
            $scope.fedit = (CRUD[2] == e) ? CRUD[2] : 0;

            u = auth.getRole(v, CRUD[3]);
            $scope.fupdate = (CRUD[3] == u) ? CRUD[3] : 0;

            rm = auth.getRole(v, CRUD[4]);
            $scope.fremove = (CRUD[4] == rm) ? CRUD[4] : 0;

        }, function (error) {
            $scope.enabled = error.data.enabled;
            $scope.message = error.data.message;
        });


    $scope.save = function () {
        var urlf = uri + CRUD[1] + "/0/";

        var formData = $scope.formDataSend();

        $http.post(urlf, formData, {
            headers: {
                "Content-type": undefined
            },
            transformRequest: angular.identity
        }).then(function (xhr) {
            location.reload();
        });
    };
    $scope.edit = function (id) {

        $scope.initEditEvent();
        $http.get(uri + CRUD[2] + "/" + id + "/")
            .then(function (xhr) {
                $scope.postEditEvent();

                $scope.product = xhr.data;
                $scope.product.category = xhr.data.categoryID._id;
                $scope.product.shop = xhr.data.shopID._id;
                $scope.product.tax = xhr.data.taxID._id;
                $scope.product.status = xhr.data.statusID._id;
                $scope.product.coins = xhr.data.coinsID._id;

                $scope.product.warehouse = xhr.data.warehouseID._id;
                $scope.product.typedef = xhr.data.typeDefID._id;

                $scope.name = xhr.data._id;
            });
    };
    $scope.upd = function (name) {
        $scope.name = name;

        var urlf = uri + CRUD[3] + "/" + $scope.name + "/";

        var formData = $scope.formDataSend();
        $http.post(urlf, formData, {
            headers: {
                "Content-type": undefined
            },
            transformRequest: angular.identity
        }).then(function (xhr) {
            location.reload();
        });

    };

    $scope.formDataSend = function () {

        var file;
        if ($scope.file == undefined) {
            file = $scope.product.image;
        } else {
            file = $scope.file;
        }
        var files = $scope.files;
        var amount = $scope.product.amount;
        var stock = $scope.product.stock;
        amount = JSON.stringify(amount);
        stock = JSON.stringify(stock);


        var formData = new FormData();

        formData.append("name", $scope.product.name);
        formData.append("code", $scope.product.code);
        formData.append("description", $scope.product.description);
        formData.append("amount", amount);
        formData.append("cost", $scope.product.cost);
        formData.append("stock", stock);
        formData.append("shop", $scope.product.shop);
        formData.append("category", $scope.product.category);
        formData.append("status", $scope.product.status);
        formData.append("available", $scope.product.available);
        formData.append("tax", $scope.product.tax);
        formData.append("coins", $scope.product.coins);
        formData.append("warehouse", $scope.product.warehouse);
        formData.append("typedef", $scope.product.typedef);
        formData.append("avatar", file);
        formData.append("avatar2", files);

        return formData;
    }

    $scope.confirm = function (id, dni) {
        $scope.deleteRecord = "Eliminar registro";
        $scope.sureDelete = "Esta seguro de eliminar";
        $scope.ofRecord = "del registro";
        $scope.yesSure = "Si";
        $scope.not = "No";
        $.confirm({
            title: $scope.deleteRecord + "",
            text: $scope.sureDelete + ' "' + dni + '" ' + $scope.ofRecord + '?',
            confirm: function () {

                $http.get(uri + CRUD[4] + "/" + id + "/")
                    .then(function () {
                        location.reload();
                    });
            },
            cancel: function () {
                // cancel
            },
            confirmButton: $scope.yesSure,
            cancelButton: $scope.not
        });
    };

    $http.get(uri + CRUD[0] + "/0/").then(function (response) {
        $scope.data = response.data;
    });

    $http.get(uris + CRUD[0] + "/0/").then(function (response) {
        $('#shopx').select2();
        $('#shopx1').select2();
        $scope.shops = response.data;
    });

    $http.get(uric + CRUD[0] + "/0/").then(function (response) {
        $('#categoryx').select2();
        $('#categoryx1').select2();
        $scope.categorys = response.data;
    });

    $http.get(urit + CRUD[0] + "/0/").then(function (response) {
        $('#taxx').select2();
        $('#taxx1').select2();
        $scope.taxs = response.data;
    });

    $http.get(urist + CRUD[0] + "/0/").then(function (response) {
        $('#statusx').select2();
        $('#statusx1').select2();
        $scope.status = response.data;
    });

    $http.get(uricoins + CRUD[0] + "/0/").then(function (response) {
        $('#coinsx').select2();
        $('#coinsx1').select2();
        $scope.coinss = response.data;
    });

    $http.get(uriwh + CRUD[0] + "/0/").then(function (response) {
        $('#wh').select2();
        $('#wh1').select2();
        $scope.warehouses = response.data;
    });

    $http.get(uritd + CRUD[0] + "/0/").then(function (response) {
        $('#td').select2();
        $('#td1').select2();
        $scope.typedefs = response.data;
    });

    // $scope.model.Logo=undefined;
    $scope.uploadFile = function (input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {

                $('#photo-id').attr('src', e.target.result);
                var canvas = document.createElement("canvas");
                var imageElement = document.createElement("img");

                imageElement.setAttribute = $('<img>', {src: e.target.result});
                var context = canvas.getContext("2d");
                imageElement.setAttribute.load(function () {
                    // debugger;
                    canvas.width = this.width;
                    canvas.height = this.height;


                    context.drawImage(this, 0, 0);
                    var base64Image = canvas.toDataURL("image/png");

                    var data = base64Image.replace(/^data:image\/\w+;base64,/, "");

                    $scope.model.Logo = data;
                });

            }
        }
    }

    $scope.files = null;
    $scope.uploadFile2 = function (input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {

                $('#photo-id2').attr('src', e.target.result);
                var canvas = document.createElement("canvas");
                var imageElement = document.createElement("img");

                imageElement.setAttribute = $('<img>', {src: e.target.result});
                var context = canvas.getContext("2d");
                imageElement.setAttribute.load(function () {
                    // debugger;
                    canvas.width = this.width;
                    canvas.height = this.height;


                    context.drawImage(this, 0, 0);
                    var base64Image = canvas.toDataURL("image/png");

                    var data = base64Image.replace(/^data:image\/\w+;base64,/, "");
                    // console.log("..../ base64 "+data);
                    $scope.files = data;
                    console.log("..../ base64v " + $scope.files);
                    $scope.model.Logo2 = data;

                    // console.log("..../ base64 "+$scope.model.Logo2);
                });

            }
        }
    }

});

