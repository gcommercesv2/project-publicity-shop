/**
 * Created by maria garcia on 27-08-2017.
 */
app.controller("reportCrt", function ($scope, $window, $http, API_URL, $localStorage, auth, API_URL_IMG, CRUD) {
    $scope.title = "Reportes";
    auth.isToken();
    auth.session();
    var brw = new Browser();
    //$scope.user = $localStorage.name;
    //const uriinfoAvatar = API_URL + "api/infoAvatar/";
    //
    //$http.get(uriinfoAvatar).then(function (response) {
    //    $scope.imgAvatar = API_URL_IMG + response.data.image;
    //});
    $scope.domain = API_URL_IMG;
    const uriprd = API_URL + "api/product/";
    $scope.dataTableProduct = false;
    $scope.opProduct = 1;
    $scope.viewframe = 0;
    $scope.openProduct = function () {
        $scope.titleReport = "Product";
        $scope.dataTableProduct = true;
    };

    $scope.pdfProduct = function () {
        $('#myModal').modal();
        $scope.viewframe = 1;

        var html2obj = html2canvas(document.getElementById('rptProduct'), {
            useCORS: true,
            logging: true
            //,
            //allowTaint:false
        });

        var queue = html2obj.parse();
        var canvas = html2obj.render(queue);
        var img = canvas.toDataURL();

        var doc = new jsPDF({
            unit: 'px',
            format: 'a4'
        });

        doc.addImage(img, 'PNG', 10, 10);

        var string = doc.output('datauristring');
        //string=string+'?#toolbar=0';
        $('iframe').attr('src', string);
    }

    $scope.pdfProducts = function () {
        $('#myModal').modal();
        $scope.viewframe = 1;
        var doc = new jsPDF('p', 'pt');

        var elem = document.getElementById('table');
        var imgElements = document.querySelectorAll('tbody img');
        var data = doc.autoTableHtmlToJson(elem);
        var images = [];
        var i = 0;
        doc.autoTable(data.columns, data.rows, {
             
            margin: {top: 60},
            drawCell: function(cell, opts) {

                //if (opts.column.dataKey === 2) {  || if (opts.column.dataKey === 0 && i < data.rows.length) {
                if (opts.column.dataKey === 2) {
                    //console.log("img "+imgElements[i].src);
                    images.push({
                        url: getBase64Image(imgElements[i], i),
                        x: cell.textPos.x,
                        y: cell.textPos.y
                    });
                    i++;
                }
            },
            addPageContent: function() {
                for (var i = 0; i < images.length; i++) {
                   // console.log("img "+images[i].url);
                    if (!images[i].isLoad) {
                        doc.addImage(images[i].url, images[i].x, images[i].y, 20, 20);
                        images[i].isLoad = true;
                    }
                }
            }
        });

        var string = doc.output('datauristring');
        //string=string+'?#toolbar=0';
        $('iframe').attr('src', string);
    }

    $scope.rpProduct = function () {
        $http.get(uriprd + CRUD[0] + "/0/").then(function (response) {
            $scope.dataProduct = response.data;

        });
    }

    $scope.rpProduct();


});

function getBase64Image(img, i) {
    var dataURL = null;
    // Create an empty canvas element
    var canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);

    // Get the data-URL formatted image
    // Firefox supports PNG and JPEG. You could check img.src to guess the
    // original format, but be aware the using "image/jpg" will re-encode the image.
    try {
        dataURL = canvas.toDataURL();
    } catch (err) {
        alert(i + ': ' + err.message);
    }

    return dataURL;
}
//<input type="file" onchange="previewFile()"><br>
//<img src="" height="200" alt="Image preview...">
function previewFile() {
    var preview = document.querySelector('img');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}

//$scope.pdfProducts1 = function () {
//    try {
//        $('#myModal').modal();
//        $scope.viewframe = 1;
//        var pdf = new jsPDF('p', 'pt', 'letter');
//
//        //Estadísticas
//        var tablaDatos = $('.table');
//
//        var data = pdf.autoTableHtmlToJson(tablaDatos[0]);
//        var imgElements = tablaDatos[0].querySelectorAll('tbody img');
//        var images = [];
//        var i = 0;
//
//        pdf.autoTable(data.columns, data.rows, {
//            startY: 550,
//            margin: {
//                right: 50,
//                left: 50
//            },
//            tableWidth: 100,
//            styles: {
//                overflow: 'linebreak',
//                columnWidth: 'wrap'
//            },
//            drawCell: function (cell, opts) {
//                //if (opts.column.dataKey === 5) {  || if (opts.column.dataKey === 0 && i < data.rows.length) {
//                if (opts.column.dataKey === 5) {
//                    images.push({
//                        url: getBase64Image(imgElements[i], i),
//                        x: cell.textPos.x,
//                        y: cell.textPos.y
//                    });
//                    i++;
//                }
//            },
//            addPageContent: function () {
//                for (var i = 0; i < images.length; i++) {
//                    if (!images[i].isLoad) {
//                        pdf.addImage(images[i].url, images[i].x, images[i].y - 4, 200, 200);
//                        images[i].isLoad = true;
//                    }
//                }
//            }
//        });
//
//        //save
//        var string = pdf.output('datauristring');
//        //string=string+'?#toolbar=0';
//        $('iframe').attr('src', string);
//    } catch (err) {
//        alert(err.message);
//    }
//}
//var doc = new jsPDF('p', 'pt');
//
//var elem = document.getElementById('table');
//var imgElements = document.querySelectorAll('#table tbody img');
//var data = doc.autoTableHtmlToJson(elem);
//var images = [];
//doc.autoTable(data.columns, data.rows, {
//    //bodyStyles: {rowHeight: 30},
//    drawCell: function (cell, opts) {
//        if (opts.column.dataKey === 5) {
//            var img = imgElements[opts.row.index];
//            images.push({
//                elem: img,
//                x: cell.textPos.x,
//                y: cell.textPos.y
//            });
//        }
//    },
//    addPageContent: function () {
//        for (var i = 0; i < images.length; i++) {
//            console.log("img " + images[i].elem);
//            doc.addImage(images[i].elem, 'png', images[i].x, images[i].y);
//            images[i].isLoad = true;
//        }
//    }
//});

//doc.save("table.pdf");
//function exportarPDF() {
//    try {
//
//        var pdf = new jsPDF('p', 'pt', 'letter');
//
//        //Estadísticas
//        var tablaDatos = $('.tb-info');
//
//        var data = pdf.autoTableHtmlToJson(tablaDatos[0]);
//        var imgElements = tablaDatos[0].querySelectorAll('tbody img');
//        var images = [];
//        var i = 0;
//
//        pdf.autoTable(data.columns, data.rows, {
//            startY: 550,
//            margin: {
//                right: 50,
//                left: 50
//            },
//            tableWidth: 500,
//            styles: {
//                overflow: 'linebreak',
//                columnWidth: 'wrap'
//            },
//            columnStyles: {
//                0: {
//                    columnWidth: 30
//                },
//                1: {
//                    columnWidth: 350
//                }
//            },
//            drawCell: function(cell, opts) {
//                if (opts.column.dataKey === 0 && i < data.rows.length) {
//                    images.push({
//                        url: getBase64Image(imgElements[i], i),
//                        x: cell.textPos.x,
//                        y: cell.textPos.y
//                    });
//                    i++;
//                }
//            },
//            addPageContent: function() {
//                for (var i = 0; i < images.length; i++) {
//                    if (!images[i].isLoad) {
//                        pdf.addImage(images[i].url, images[i].x, images[i].y - 4, 20, 20);
//                        images[i].isLoad = true;
//                    }
//                }
//            }
//        });
//
//        //save
//        pdf.save('reporte.pdf');
//    } catch (err) {
//        alert(err.message);
//    }
//
//};