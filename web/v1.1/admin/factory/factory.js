app.factory('auth', ['$localStorage', '$http', 'API_URL', '$q', function ($localStorage, $http, API_URL) {
    return {
        isToken: function () {
            if ($localStorage.token == undefined || $localStorage.token == null) {
                window.location = "/login";
            }
        },
        isLoggedUser: function(){
            if ($localStorage.token != undefined) {
                window.location = "/admin";
            }
        },
        session: function () {
            var main = "api/";
            var url = API_URL + main + "auth/";
            $http.defaults.xsrfCookieName = 'Authorization';
            $http.defaults.xsrfHeaderName = 'Authorization';
            $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;

            $http.get(url)
                .then(function (success) {
                    $localStorage.errorSession = undefined;
                }, function (e) {
                    if (e || e == null) {
                        $localStorage.name = undefined;
                        $localStorage.email = undefined;
                        $localStorage.token = undefined;
                    }
                    $localStorage.errorSession = "Realizo un cierre de sesion inesperado";

                });

        },
        storage: function (token, email, name,id) {
            $localStorage.name = name;
            $localStorage.email = email;
            $localStorage.token = token;
            $localStorage.id=id;

        },
        getRole: function (v, args) {
            var key = args;
            var indexValue = v.findIndex(function (item, i) {
                return item === key
            });

            var vt = v[indexValue];

            return vt;
        },
        getCodigo: function () {
            const random = "abcdefghijklmnopqrstuvwxyz1234567890";
            const longcade = 5;

            function rand_code(chars, lon) {
                var code = "";
                for (var x = 0; x < lon; x++) {
                    var rand = Math.floor(Math.random() * chars.length);
                    code += chars.substr(rand, 1);
                }
                return code;
            }

            return rand_code(random, longcade);
        }

    }
}]);