/**
 * Created by maria garcia on 07-05-2017.
 */
app.config(function ($routeProvider) {

    $routeProvider       
        .when('/', {
            templateUrl: 'template/home/home.html',
            controller: 'homeCrt'
        })
        .when('/logout', {
            templateUrl: 'template/exit.html',
            controller: 'logoutCrt'
        })
        ////////////account////////////////////////////
        .when('/account', {
            templateUrl: 'template/account/home.html',
            controller: 'accountCrt'
        })
        .when('/account/tax', {
            templateUrl: 'template/account/tax.html',
            controller: 'taxCrt'
        })
        .when('/account/coins', {
            templateUrl: 'template/account/coins.html',
            controller: 'coinsCrt'
        })
        //////////settings//////////////////////////////
        .when('/settings', {
            templateUrl: 'template/settings/home.html',
            controller: 'settingsCrt'
        })
        .when('/settings/status', {
            templateUrl: 'template/settings/status.html',
            controller: 'statusCrt'
        })
        .when('/settings/category', {
            templateUrl: 'template/settings/category.html',
            controller: 'categoryCrt'
        })
        /////////geo//////////////////////////////////////
        .when('/geo', {
            templateUrl: 'template/geo/home.html',
            controller: 'geoCrt'
        })
        .when('/geo/location', {
            templateUrl: 'template/geo/location.html',
            controller: 'locationCrt'
        })
        .when('/geo/shop', {
            templateUrl: 'template/geo/shop.html',
            controller: 'shopCrt'
        })
        ///////////planning//////////////////////////////
        .when('/planning', {
            templateUrl: 'template/planning/home.html',
            controller: 'planningCrt'
        })
        .when('/planning/planner', {
            templateUrl: 'template/planning/planner.html',
            controller: 'plannerCrt'
        })
        ///////////product///////////////////////////////
        .when('/product', {
            templateUrl: 'template/product/home.html',
            controller: 'productCrt'
        })
        .when('/product/type-def', {
            templateUrl: 'template/product/typeDef.html',
            controller: 'typeDefCrt'
        })
        .when('/product/warehouse', {
            templateUrl: 'template/product/warehouse.html',
            controller: 'warehouseCrt'
        })
        .when('/product/products', {
            templateUrl: 'template/product/product.html',
            controller: 'productsCrt'
        })
        ////////sales////////////////////////////////////
        .when('/sales', {
            templateUrl: 'template/sales/home.html',
            controller: 'salesCrt'
        })
        .when('/sales/provider', {
            templateUrl: 'template/sales/provider.html',
            controller: 'providerCrt'
        })
        .when('/sales/method-payment', {
            templateUrl: 'template/sales/methodPayment.html',
            controller: 'methodPaymentCrt'
        })
        ////////security/////////////////////////////////
        .when('/security', {
            templateUrl: 'template/security/home.html',
            controller: 'securityCrt'
        })
        .when('/security/profile', {
            templateUrl: 'template/security/profile.html',
            controller: 'profileCrt'
        })
        .when('/security/list-user', {
            templateUrl: 'template/security/list-user.html',
            controller: 'listUserCrt'
        })
        .when('/security/list-roles', {
            templateUrl: 'template/security/list-roles.html',
            controller: 'listRolesCrt'
        })
         .when('/security/log-system', {
            templateUrl: 'template/security/log-system.html',
            controller: 'logSystemCrt'
        })
        /////////////report//////////////////////////////////          
        .when('/report', {
            templateUrl: 'template/report/report.html',
            controller: 'reportCrt'
        })        
        .otherwise({
            redirectTo: '/'
        });
});
