// importar
//var express = require('express');

// instanciar
//var srv = express();
//var app = express();
// var promise = require('bluebird');

// var options = {
//     // Initialization Options
//     promiseLib: promise
// };

// var pgp = require('pg-promise')(options);
// var connectionString = 'postgres://postgres:root@localhost:5432/testdjango';
// var db = pgp(connectionString);


var cors = require('cors');
var jwt = require('jwt-simple');
var bodyParser = require('body-parser');
var methodOverride = require("method-override");
var moment = require('moment');

var fs = require('fs'),
//https = require('https'),
    express = require('express'),
    app = express();
/*
 https.createServer({
 key: fs.readFileSync('key.pem'),
 cert: fs.readFileSync('cert.pem')
 }, app).listen(8100);*/
app.listen(8090);

//app.get('/', function (req, res) {
//    res.header('Content-type', 'text/html');
//    return res.end('<h1>Hello, Secure World!</h1>');
//});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({keepExtensions: true, extended: true}));
app.use(methodOverride());
app.use(express.static(__dirname));


var multipart = require('connect-multiparty');
app.use(multipart());

var Promise = require('bluebird');
var mongoose = require('mongoose');
var mongoose = Promise.promisifyAll(mongoose);

app.use(cors({
    'origin': '*'
}));

mongoose.connect('mongodb://localhost:27017/dbpublicshop', function (err, res) {
    if (err) throw err;
    console.log('Connected to Database');
});

app.get('/admin', function (req, res) {
    res.sendFile(__dirname + '/admin/template/index.html');
});

app.get('/login', function (req, res) {
    res.sendFile(__dirname + '/admin/template/login.html');
});

app.get('/signin', function (req, res) {
    res.sendFile(__dirname + '/site/template/login.html');
});

//----------------------------------------------------------------------------------

var passport = require('passport');
require('./oauth/model/Oauth');
require('./oauth/controller/OauthController')(passport);

var session = require('express-session')
var sess = {
    secret: 'SECRETS',
    cookie: {}
}

app.use(session(sess));

app.use(passport.initialize());
app.use(passport.session());

app.use('/api/auth/facebook', passport.authenticate('facebook'));
app.get('api/auth/facebook/callback', passport.authenticate('facebook',
    {
        successRedirect: '/',
        failureRedirect: '/login'
    }));

app.get('/api/auth/google', passport.authenticate('google',
    {
        successRedirect: 'http://localhost:8090/'
        , scope: ['email', 'profile']
    }));

app.get('/api/auth/google/callback', passport.authenticate('google',
    {
        
        successRedirect: '/site',
        //successRedirect: '/site/#!/load',
        //successRedirect: '/signin',
        failureRedirect: '/signin'
    }));

app.use('/api/auth/session', isLoggedIn, function (req, res, next) {

    var payload = {
        name: req.user.name,
        email: req.user.email,
        photo: req.user.photo,
        'exp': moment().add(14, "days").unix()
    }
    var token = jwt.encode(payload, 'SECRET');
    var payloads = {
        name: req.user.name,
        email: req.user.email,
        photo: req.user.photo,
        token: token
    }
    res.status(200).jsonp(payloads);
    //res.status(200).jsonp(payload);
});

app.get('/site', function (req, res) {
    res.sendFile(__dirname + '/site/template/index.html');
});

app.get('/oauth-site', function (req, res) {
    res.sendFile(__dirname + '/oauth-site/template/index.html');
});

app.get('/api/auth/logout', function (req, res) {
    // destroy the user's session to log them out
    // will be re-created next request
    req.session.destroy(function () {
        res.redirect('http://localhost:8090/signin');
    });
});

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    }
    // if they aren't redirect them to the home page
    res.redirect('http://localhost:8090/signin');
}

console.log("Servidor Express escuchando en modo %s", app.settings.env);