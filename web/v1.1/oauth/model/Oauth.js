var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Oauth', new Schema({
    // facebook         : {
    //     id           : String,
    //     token        : String,
    //     email        : String,
    //     name         : String
    // },
    // twitter          : {
    //     id           : String,
    //     token        : String,
    //     displayName  : String,
    //     username     : String
    // },
    // google           : {
    //     id           : String,
    //     token        : String,
    //     email        : String,
    //     name         : String
    // }

    name: String,
    email: String,
    provider: String,
    provider_id: {type: String, unique: true},
    photo: String,
    createdAt: {type: Date, default: Date.now}

}));