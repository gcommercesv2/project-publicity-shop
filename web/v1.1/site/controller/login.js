app.controller("loginCrt", function ($scope, $window, $http, API_URL, API_URL_LOCAL, $localStorage, auth, contentType) {
    $scope.title = "Login";
    const uri = API_URL + "api/login/";

    console.log(".... " + $localStorage.oauth2 + " .... token " + $localStorage.token);
    // if ($localStorage.token != undefined) {
    //     window.location = "/site";
    // }
    auth.isLoggedUser();
    $scope.signUp = function () {
        $scope.fail = 0;
        $http.post(uri, $scope.User, {headers: {'Content-Type': contentType}})
            .then(function (success) {
                $scope.token = success.data.token;
                if ($scope.token != undefined) {
                    auth.isOauth(false);
                    auth.storage($scope.token, success.data.email, success.data.name);

                    window.location = "/site";

                } else {

                    $scope.load = 1;
                    $scope.loader = 1;

                    $scope.fail = 1;
                    $scope.mess = "Failed credential";
                }
            }, function (error) {
                $scope.fail = 1;
                $scope.mess = error.data + " Failed credential";
            });
    }

    $scope.facebook = function () {
        auth.isOauth(true);
        window.location = API_URL_LOCAL + "api/auth/facebook";
    }

    $scope.google = function () {
        auth.isOauth(true);
        window.location = API_URL_LOCAL + "api/auth/google";
        //auth.sessionOauth();
    }

    //auth.sessionOauth();

});

app.controller("registerCrt", function ($scope, $window, $http, API_URL, $localStorage) {
    $scope.title = "Login";
    const uri = API_URL + "api/register/";
    var contentType = 'application/json';
    $scope.register = function () {
        $http.post(uri, $scope.User, {headers: {'Content-Type': contentType}})
            .then(function (success) {
                window.location = "/signin";
            }, function (error) {
            });
    }

});