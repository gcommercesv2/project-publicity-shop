app.factory('auth', ['$localStorage', '$http', 'API_URL', 'API_URL_LOCAL', '$q', function ($localStorage, $http, API_URL, API_URL_LOCAL, $q) {
    return {
        isToken: function () {
            if ($localStorage.token == undefined || $localStorage.token == null) {
                window.location = "/signin";
            }
        },
        isLoggedUser: function(){
            if ($localStorage.token != undefined) {
                window.location = "/site";
            }
        },
        session: function () {
            var main = "api/";
            var url = API_URL + main + "auth/";
            $http.defaults.xsrfCookieName = 'Authorization';
            $http.defaults.xsrfHeaderName = 'Authorization';
            $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;

            $http.get(url)
                .then(function (success) {
                    $localStorage.errorSession = undefined;

                }, function (e) {
                    if (e || e == null) {
                        $localStorage.name = undefined;
                        $localStorage.email = undefined;
                        $localStorage.token = undefined;
                        $localStorage.oauth2 = undefined;
                    }
                    $localStorage.errorSession = "Realizo un cierre de sesion inesperado";

                });

        },
        sessionOauthBearer:function(){
            //console.log("success token .... "+$localStorage.token+"... \n");
            $http.defaults.xsrfCookieName = 'Authorization';
            $http.defaults.xsrfHeaderName = 'Authorization';
            $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
        },
        sessionOauth: function () {
            var main = "api/";
            var urli = API_URL_LOCAL + main + "auth/session";

            $http.get(urli)
                .then(function (success) {
                   // console.log("success "+JSON.stringify(success.data)+"... \n");
                    $localStorage.name = success.data.name;
                    $localStorage.email = success.data.email;
                    $localStorage.photo = success.data.photo;
                    $localStorage.token = success.data.token;

                    $http.defaults.xsrfCookieName = 'Authorization';
                    $http.defaults.xsrfHeaderName = 'Authorization';
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;

                }, function (e) {
                    if (e || e == null) {
                        $localStorage.name = undefined;
                        $localStorage.email = undefined;
                        $localStorage.token = undefined;
                        $localStorage.oauth2 = undefined;
                    }
                    $localStorage.errorSession = "Realizo un cierre de sesion inesperado";

                });
        },
        asyncAuthSession:function(){
            var deferred = $q.defer();
            var promise = deferred.promise;
            var main = "api/";
                var urli = API_URL_LOCAL + main + "auth/session";
                $http.get(urli).then(function (success) {
                    $localStorage.token=success.data.token;
                    $http.defaults.xsrfCookieName = 'Authorization';
                    $http.defaults.xsrfHeaderName = 'Authorization';
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
                    deferred.resolve(success);
                }, function (e) {
                    deferred.reject(e);
                });
                return promise;
        },
        sessionX:function(){
            if ($localStorage.oauth2 == true) {
                 
                if ($localStorage.email == undefined) {
                    location.reload();                
                }  
            } else if ($localStorage.oauth2 == false) {          
                
            }
        },
        refreshOauth: function () {
            location.reload();
        },
        storage: function (token, email, name) {
            $localStorage.name = name;
            $localStorage.email = email;
            $localStorage.token = token;

        },
        isOauth: function (oauth) {
            $localStorage.oauth2 = oauth;
        },
        getRole: function (v, args) {
            var key = args;
            var indexValue = v.findIndex(function (item, i) {
                return item === key
            });

            var vt = v[indexValue];

            return vt;
        },
        getCodigo: function () {
            const random = "abcdefghijklmnopqrstuvwxyz1234567890";
            const longcade = 5;

            function rand_code(chars, lon) {
                var code = "";
                for (var x = 0; x < lon; x++) {
                    var rand = Math.floor(Math.random() * chars.length);
                    code += chars.substr(rand, 1);
                }
                return code;
            }

            return rand_code(random, longcade);
        }

    }
}]);

// app.service('srvOauth'['$localStorage', '$http', 'API_URL','API_URL_LOCAL', '$q', function ($localStorage, $http, API_URL,API_URL_LOCAL) {
//     // this.myFunc = function (x) {
//     //     return x.toString(16);
//     // }
//     this.sessionOauth=function(){
//         var main = "api/";
//             var url = API_URL + main + "auth/";
//             $http.defaults.xsrfCookieName = 'Authorization';
//             $http.defaults.xsrfHeaderName = 'Authorization';
//             $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;

//             $http.get(url)
//                 .then(function (success) {
//                     $localStorage.name = success;
//                         $localStorage.email = undefined;
//                         $localStorage.token = undefined;
//                 }, function (e) {
//                     if (e || e == null) {
//                         $localStorage.name = undefined;
//                         $localStorage.email = undefined;
//                         $localStorage.token = undefined;
//                     }
//                     $localStorage.errorSession = "Realizo un cierre de sesion inesperado";

//             });

//     }

// }]);

/*
sessionOauth: function () {
            var main = "api/";
            var urli = API_URL_LOCAL + main + "auth/session";

            var deferred = $q.defer();
            var promise = deferred.promise;
            //asyncAuthSession()
            $http.get(urli)
                .then(function (success) {
                    console.log("success "+JSON.stringify(success.data));
                    $localStorage.name = success.data.name;
                    $localStorage.email = success.data.email;
                    $localStorage.photo = success.data.photo;
                    $localStorage.token = success.data.token;

                    $http.defaults.xsrfCookieName = 'Authorization';
                    $http.defaults.xsrfHeaderName = 'Authorization';
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;

                }, function (e) {
                    if (e || e == null) {
                        $localStorage.name = undefined;
                        $localStorage.email = undefined;
                        $localStorage.token = undefined;
                        $localStorage.oauth2 = undefined;
                    }
                    $localStorage.errorSession = "Realizo un cierre de sesion inesperado";

                });

            // function asyncAuthSession(){
            //     var main = "api/";
            //     var urli = API_URL_LOCAL + main + "auth/session";
            //     $http.get(urli).then(function (success) {
            //         $http.defaults.xsrfCookieName = 'Authorization';
            //         $http.defaults.xsrfHeaderName = 'Authorization';
            //         $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
            //         deferred.resolve(success);
            //     }, function (e) {
            //         deferred.reject(e);
            //     });
            //     return promise;
            // }


        },

*/