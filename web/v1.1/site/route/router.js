/**
 * Created by maria garcia on 07-05-2017.
 */
app.config(function ($routeProvider) {

    $routeProvider
        //.when('/login', {
        //    templateUrl: 'template/login/login.html',
        //    controller: 'loginCrt'
        ////})

        .when('/', {
            templateUrl: 'template/home/home.html',
            controller: 'homeCrt'
        })
        .when('/load', {
            templateUrl: 'template/home/load.html',
            controller: 'loadCrt'
        })
        .when('/logout', {
            templateUrl: 'template/exit.html',
            controller: 'logoutCrt'
        })
        .when('/security', {
            templateUrl: 'template/security/security.html',
            controller: 'securityCrt'
        })
        .when('/settings', {
            templateUrl: 'template/security/settings.html',
            controller: 'settingsCrt'
        })
        .when('/list-user', {
            templateUrl: 'template/security/list-user.html',
            controller: 'listUserCrt'
        })
        .when('/list-roles', {
            templateUrl: 'template/security/list-roles.html',
            controller: 'listRolesCrt'
        })
        .when('/shop', {
            templateUrl: 'template/product/shop.html',
            controller: 'shopCrt'
        })
        .when('/report', {
            templateUrl: 'template/report/report.html',
            controller: 'reportCrt'
        })
        .when('/category', {
            templateUrl: 'template/product/category.html',
            controller: 'categoryCrt'
        })
        .when('/product', {
            templateUrl: 'template/product/productInfo.html',
            controller: 'productInfoCrt'
        })
        .otherwise({
            redirectTo: '/'
        });
});
