var ApiUrl = require('../model/ApiUrl');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

exports.apiUrlCRUD = function (prm, id, req, res, next) {

    var name;
    var link;

    var payload;

    var t = middle.middlewareRolWSBackend("admin", req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            ApiUrl.find(function (err, Roless) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /ApiUrl');
                return res.status(200).jsonp(Roless);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = req.param('name', null);
            link = req.param('link', null);

            payload = {name: name, link: link};
            ApiUrl.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new ApiUrl(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            ApiUrl.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            name = req.param('name', null);
            link = req.param('link', null);

            payload = {name: name, link: link};
            ApiUrl.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            ApiUrl.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }
    });
}
