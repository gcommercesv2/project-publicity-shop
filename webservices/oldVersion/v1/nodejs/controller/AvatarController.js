var Avatar = require('../model/Avatar');
var User = require('../model/User');
var crud = require('../utility/utility');
var fs = require('fs');
var middle = require('../middleware/middleware');

exports.avatarCRUD = function (prm, id, req, res, next) {

    var userID;
    var email;
    var image;
    var payload;
    var tmp_path;
    var target_path;

    var t = middle.middlewareRolWSBackend("user", req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Avatar.find().populate('userID').exec(function (err, products) {
                if (err) {
                    res.send(500, err.message);
                }
                // console.log('GET /product');
                return res.status(200).jsonp(products);
            });

        } else if (prm == crud.vectorSave(vektor)) {
            email = req.param('email', null);
            image;
            User.findOne({email: email}, function (err, prod) {
                try {

                    tmp_path = req.files.avatar.path;
                    target_path = './upload/' + req.files.avatar.name;
                    if (req.files.avatar.type.indexOf('image') == -1) {
                        res.send({message: "this file is not image...!!!"});
                    } else {
                        fs.rename(tmp_path, target_path, function (err) {
                            if (err) throw err;
                            fs.unlink(tmp_path, function () {
                                if (err) throw err;
                            });
                        });
                    }

                    image = '/upload/' + req.files.avatar.name;
                } catch (err) {
                    image = req.param('avatar', null);
                }
                userID = prod._id;
                console.log("---erp " + userID);
                payload = {
                    userID: userID,
                    image: image
                };

                console.log("---er " + payload);
                var prd = new Avatar(payload);
                prd.save(function (err, p) {
                    if (err) return res.send(500, err.message);
                    res.status(200).jsonp(p);
                });


            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Avatar.findOne({_id: id}).populate('userID').exec(function (err, prod) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(prod);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            email = req.param('email', null);
            image;

            User.findOne({email: email}, function (err, prod) {
                try {

                    tmp_path = req.files.avatar.path;
                    target_path = './upload/' + req.files.avatar.name;
                    if (req.files.avatar.type.indexOf('image') == -1) {
                        res.send({message: "this file is not image...!!!"});
                    } else {
                        fs.rename(tmp_path, target_path, function (err) {
                            if (err) throw err;
                            fs.unlink(tmp_path, function () {
                                if (err) throw err;
                            });
                        });
                    }

                    image = '/upload/' + req.files.avatar.name;
                } catch (err) {
                    image = req.param('avatar', null);
                }

                userID = prod._id;
                payload = {
                    userID: userID,
                    image: image
                };

                Avatar.updateOne({_id: id}, {$set: payload}, function (err, usr) {
                    if (err) return res.send(500, err.message);
                    res.status(200).jsonp(usr);
                });
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Avatar.remove({_id: id}, function (err, prod) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(prod);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}


exports.addAvatar = function (req, res, next) {
    var email, image, userID, payload, tmp_path, target_path;
    email = req.param('email', null);
    image;

    User.findOne({email: email}, function (err, prod) {
        try {

            tmp_path = req.files.avatar.path;
            target_path = './upload/' + req.files.avatar.name;
            if (req.files.avatar.type.indexOf('image') == -1) {
                res.send({message: "this file is not image...!!!"});
            } else {
                fs.rename(tmp_path, target_path, function (err) {
                    if (err) throw err;
                    fs.unlink(tmp_path, function () {
                        if (err) throw err;
                    });
                });
            }

            image = '/upload/' + req.files.avatar.name;
        } catch (err) {
            image = req.param('avatar', null);
        }

        userID = prod._id;
        payload = {
            userID: userID,
            image: image
        };

        Avatar.findOne({userID: userID}, function (err, prod) {
            if (prod == null || prod == undefined) {
                var prd = new Avatar(payload);
                prd.save(function (err, p) {
                    if (err) return res.send(500, err.message);
                    res.status(200).jsonp(p);
                });
            } else {
                payload = {
                    image: image
                };
                Avatar.updateOne({userID: userID}, {$set: payload}, function (err, usr) {
                    if (err) return res.send(500, err.message);
                    res.status(200).jsonp(usr);
                });
            }
        });
    });
}