var Category = require('../model/Category');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

exports.categoryCRUD = function (rol,prm, id, req, res, next) {

    var name;
    var description;
    var payload;
    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Category.findAsync(function (err, categorys) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Category');
                return res.status(200).jsonp(categorys);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = req.param('name', null);
            description = req.param('description', null);

            payload = {name: name, description: description};
            Category.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Category(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Category.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            name = req.param('name', null);
            description = req.param('description', null);

            payload = {name: name, description: description};
            Category.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Category.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}
