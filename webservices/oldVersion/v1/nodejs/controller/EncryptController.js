var utl = require('../utility/utility');
var jwt = require('jwt-simple');
var moment = require('moment');

exports.chiperData = function (prm, req, res, next) {
    var payloads;
    var enc;
    var dec;
    if (prm == "encode") {
        payloads = req.param('payload', null);
        enc = {
            'payload': payloads,
            'exp': moment().add(14, "days").unix()
        };
        var token = jwt.encode(enc, 'SECRET');
        var deco=jwt.decode(token,'SECRET');
        var ip = utl.getIp(req);
        var headers=utl.getHeadersSniffer(req,res,next);
        //console.log("dec---- "+JSON.stringify(deco.payload)+" --- ip: "+ip+" ----- headers: "+headers);
        res.jsonp({"encode": token});

    } else if (prm == "decode") {
        payloads = req.param('payload', null);
        var token= jwt.decode(payloads,'SECRET');
        dec=token.payload;
        res.send({"dec":dec})
    }
}

 