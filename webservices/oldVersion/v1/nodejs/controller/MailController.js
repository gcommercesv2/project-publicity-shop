// const nodemailer = require('nodemailer');

// // create reusable transporter object using the default SMTP transport
// let transporter = nodemailer.createTransport({
//     host: 'smtp.example.com',
//     port: 465,
//     secure: true, // secure:true for port 465, secure:false for port 587
//     auth: {
//         user: 'username@example.com',
//         pass: 'userpass'
//     }
// });

// // setup email data with unicode symbols
// let mailOptions = {
//     from: '"Fred Foo " <foo@blurdybloop.com>', // sender address
//     to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
//     subject: 'Hello ✔', // Subject line
//     text: 'Hello world ?', // plain text body
//     html: '<b>Hello world ?</b>' // html body
// };

// // send mail with defined transport object
// transporter.sendMail(mailOptions, (error, info) => {
//     if (error) {
//         return console.log(error);
//     }
//     console.log('Message %s sent: %s', info.messageId, info.response);
// });

var nodemailer = require('nodemailer');
// email sender function
exports.sendEmail = function (req, res) {
// Definimos el transporter
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        // auth: {
        //     user: 'adelgado@broadland.group',
        //     pass: '18149483'
        // }
        auth: {
            type: 'OAuth2',
            user: 'adelgado@broadland.group',
            clientId: '723935320367-prv0rv5js90vaiugae8n1bqpgsbm984m.apps.googleusercontent.com',
            clientSecret: 'aLKGQ0hDK_et7ZeuJ0n-uIcz',
            refreshToken: '1/3P1niCItNTiE9pf38JxDtT2rtWBk-MEuPWVFO0Eeg9c'
        }
    });
// Definimos el email
    var mailOptions = {
        from: '"Fred Foo " <adelgado@broadland.group>',
        to: 'anderson.andersondelgado.delga@gmail.com',
        subject: 'Prueba',
        text: 'Esto es una prueba'
    };
// Enviamos el email
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            //res.send(500, err.message);
            res.send(500, error.message);
        } else {
            console.log("Email sent");
            res.status(200).jsonp(req.body);
        }
    });

};

exports.sendEmailTo = function (req, res) {
// Definimos el transporter
    var usermail = req.param('email', null);
    var rass = req.param('subject', null);
    var content = req.param('content', null);
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        // auth: {
        //     user: 'adelgado@broadland.group',
        //     pass: '18149483'
        // }
        auth: {
            type: 'OAuth2',
            user: 'adelgado@broadland.group',
            clientId: '723935320367-prv0rv5js90vaiugae8n1bqpgsbm984m.apps.googleusercontent.com',
            clientSecret: 'aLKGQ0hDK_et7ZeuJ0n-uIcz',
            refreshToken: '1/3P1niCItNTiE9pf38JxDtT2rtWBk-MEuPWVFO0Eeg9c'
        }
    });
// Definimos el email
    var mailOptions = {
        from: '"adelgado " <adelgado@broadland.group>',
        to: usermail,
        subject: rass,
        text: content
    };
// Enviamos el email
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            //res.send(500, err.message);
            res.send(500, error.message);
        } else {
            console.log("Email sent");
            res.status(200).jsonp(req.body);
        }
    });

};

exports.sendEmailNoReply = function (req, res) {
// Definimos el transporter
    var usermail = req.param('email', null);
    //------------------------------------
    //var rass=req.param('subject',null);
    //var content=req.param('content',null);
    //-------------------------------------
    var rass = 'Urgente';
    var content = 'Hola, estamos haciendo pruebas de concepto.';

    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        // auth: {
        //     user: 'adelgado@broadland.group',
        //     pass: '18149483'
        // }
        auth: {
            type: 'OAuth2',
            user: 'adelgado@broadland.group',
            clientId: '723935320367-prv0rv5js90vaiugae8n1bqpgsbm984m.apps.googleusercontent.com',
            clientSecret: 'aLKGQ0hDK_et7ZeuJ0n-uIcz',
            refreshToken: '1/3P1niCItNTiE9pf38JxDtT2rtWBk-MEuPWVFO0Eeg9c'
        }
    });
// Definimos el email
    var mailOptions = {
        from: '"adelgado " <adelgado@broadland.group>',
        to: usermail,
        subject: rass,
        text: content
    };
// Enviamos el email
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            //res.send(500, err.message);
            res.send(500, error.message);
        } else {
            console.log("Email sent");
            res.status(200).jsonp(req.body);
        }
    });

};

exports.sendEmailMulti = function (req, res) {
// Definimos el transporter
    //var usermail = req.param('email', null);
    var multi = req.param('multi', null);
    //------------------------------------
    // var rass=req.param('subject',null);
    // var content=req.param('content',null);
    //-------------------------------------
    var rass = 'Urgente';
    var content = 'Hola, estamos haciendo pruebas de concepto.';

    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        // auth: {
        //     user: 'adelgado@broadland.group',
        //     pass: '18149483'
        // }
        auth: {
            type: 'OAuth2',
            user: 'adelgado@broadland.group',
            clientId: '723935320367-prv0rv5js90vaiugae8n1bqpgsbm984m.apps.googleusercontent.com',
            clientSecret: 'aLKGQ0hDK_et7ZeuJ0n-uIcz',
            refreshToken: '1/3P1niCItNTiE9pf38JxDtT2rtWBk-MEuPWVFO0Eeg9c'
        }
    });


// Definimos el email
    var mailOptions = {
        from: '"adelgado " <adelgado@broadland.group>',
        to: multi,
        subject: rass,
        text: content
    };
// Enviamos el email
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            //res.send(500, err.message);
            res.send(500, error.message);
        } else {
            console.log("Email sent");
            res.status(200).jsonp(req.body);
        }
    });

};