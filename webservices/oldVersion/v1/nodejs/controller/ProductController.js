var Product = require('../model/Product');

var jwt = require('jwt-simple');
var crud = require('../utility/utility');
var fs = require('fs');
var middle = require('../middleware/middleware');

exports.productCRUD = function (rol, prm, id, req, res, next) {

    var name;
    var code;
    var description;
    var categoryID;
    var shopID;
    var taxID;
    var coinsID;
    var amount;
    var stock;
    var statusID;
    var image;
    var payload;
    var available;
    var tmp_path;
    var target_path;

    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Product.find()
                .populate('categoryID')
                .populate('shopID')
                .populate('taxID')
                .populate('statusID')
                .populate('coinsID').exec(function (err, products) {
                if (err) {
                    res.send(500, err.message);
                }
                // console.log('GET /product');
                return res.status(200).jsonp(products);
            });

        } else if (prm == crud.vectorSave(vektor)) {
            name = req.param('name', null);
            code = req.param('code', null);
            description = req.param('description', null);
            categoryID = req.param('category', null);
            shopID = req.param('shop', null);
            amount = req.param('amount', null);
            available = req.param('available', null);
            stock = req.param('stock', null);
            statusID = req.param('status', null);
            coinsID = req.param('coins', null);
            taxID = req.param('tax', null);
            image;

            amount = JSON.parse(amount);
            stock = JSON.parse(stock);

            try {
                console.log("file..... " + JSON.stringify(req.files));
                tmp_path = req.files.avatar.path;
                target_path = './upload/' + req.files.avatar.name;
                if (req.files.avatar.type.indexOf('image') == -1) {
                    res.send({message: "this file is not image...!!!"});
                } else {
                    fs.rename(tmp_path, target_path, function (err) {
                        if (err) throw err;
                        fs.unlink(tmp_path, function () {
                            if (err) throw err;
                        });
                    });
                }

                image = '/upload/' + req.files.avatar.name;
            } catch (err) {
                image = req.param('avatar', null);
            }

            payload = {
                code: code,
                name: name,
                description: description,
                image: image,
                categoryID: categoryID,
                shopID: shopID,
                amount: amount,
                statusID: statusID,
                available: available,
                coinsID: coinsID,
                taxID: taxID,
                stock: stock
            };

            Product.findOne({name: name}, function (err, prod) {

                if (prod == null || prod == undefined) {

                    var prd = new Product(payload);
                    prd.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Product.findOne({_id: id})
                .populate('categoryID')
                .populate('shopID')
                .populate('taxID')
                .populate('statusID')
                .populate('coinsID').exec(function (err, prod) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(prod);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            name = req.param('name', null);
            code = req.param('code', null);
            description = req.param('description', null);
            categoryID = req.param('category', null);
            shopID = req.param('shop', null);
            amount = req.param('amount', null);
            available = req.param('available', null);
            stock = req.param('stock', null);
            statusID = req.param('status', null);
            coinsID = req.param('coins', null);
            taxID = req.param('tax', null);
            image;

            amount = JSON.parse(amount);
            stock = JSON.parse(stock);
            try {

                tmp_path = req.files.avatar.path;
                target_path = './upload/' + req.files.avatar.name;
                if (req.files.avatar.type.indexOf('image') == -1) {
                    res.send({message: "this file is not image...!!!"});
                } else {
                    fs.rename(tmp_path, target_path, function (err) {
                        if (err) throw err;
                        fs.unlink(tmp_path, function () {
                            if (err) throw err;
                        });
                    });
                }

                image = '/upload/' + req.files.avatar.name;
            } catch (err) {
                image = req.param('avatar', null);
            }

            payload = {
                code: code,
                name: name,
                description: description,
                image: image,
                categoryID: categoryID,
                shopID: shopID,
                available: available,
                amount: amount,
                statusID: statusID,
                coinsID: coinsID,
                taxID: taxID,
                stock: stock
            };

            Product.updateOne({_id: id}, {$set: payload}, function (err, usr) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(usr);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Product.remove({_id: id}, function (err, prod) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(prod);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}


exports.productInfo = function (req, res, next) {
    Product.find().populate('categoryID')
        .populate('shopID')
        .populate('taxID')
        .populate('statusID')
        .populate('coinsID').exec(function (err, products) {
        if (err) {
            res.send(500, err.message);
        }
        // console.log('GET /product');
        return res.status(200).jsonp(products);
    });
}

exports.productInfoDetail = function (id, req, res, next) {
    try {
        Product.findOne({_id: id})
            .populate('categoryID')
            .populate('shopID')
            .populate('taxID')
            .populate('coinsID').exec(function (err, products) {
            if (err) {
                res.send(500, err.message);
            }
            // console.log('GET /product');
            return res.status(200).jsonp(products);
        });
    } catch (e) {
        res.send(500, e.message);
    }
}

exports.validPaymentProduct = function (req, res, next) {
    var enc;
    var dec;
    var pay;
    var total = 0;
    var tax = 0;
    var price = 0;
    var sums = 0;
    var amountPrice = 0;
    var taxAmount = 0;
    var status;
    var available = 0;
    var valueStatus = 0;
    try {
        enc = req.param('encode', null);
        dec = jwt.decode(enc, 'SECRET');
        pay = dec.payload;

        pay.forEach(function (i) {
            process.nextTick(function () {

                Product.find({_id: i._id})
                    .populate('categoryID')
                    .populate('shopID')
                    .populate('taxID')
                    .populate('coinsID').exec(function (err, products) {

                    if (err) {
                        res.send(500, err.message);
                    }

                    products.forEach(function (j) {

                        var amountPrices = j.amount.price.toFixed(2);
                        status = {"name": "pendiente", "type": valueStatus};

                        if (i.price != amountPrices) {
                            // res.status(500).send({"message": "cambios en precios"});
                            //throw new Error('cambios en precios')
                            console.log("message: cambios en precios")
                        } else if (i.cant == 0) {
                            // res.status(500).send({"message": "Debe pedir al menos un producto"});
                            // throw new Error('Debe pedir al menos un producto')
                            console.log("message: Debe pedir al menos un producto")
                        } else if (i.cant > j.available) {
                            //res.status(500).send({"message": "no puede ser mayor a la solicitada"});
                            //throw new Error('no puede ser mayor a la solicitada')
                            console.log("message : no puede ser mayor a la solicitada")
                        } else if (j.available <= j.stock.min || j.stock.min == i.cant) {
                            // res.status(500).send({"message": "no puede ser mayor a la solicitada"});
                            //throw new Error('no puede ser mayor a la solicitada debe respetar el stock minimo')
                            console.log("message: no puede ser mayor a la solicitada. Agotado")
                        } else {
                            available = j.available - i.cant;
                            console.log("available: " + available);
                            var prupdate = {
                                available: available
                                //,
                                // status:status
                            };
                            saleProduct(i._id, prupdate);
                        }

                    });
                });

            });

            amountPrice = parseFloat(i.price);
            taxAmount = parseFloat(i.tax);
            sums = parseFloat(i.cant);
            tax = ((amountPrice)) * ((taxAmount));
            price = ((amountPrice)) + tax;
            price = (price) * ((sums));
            total = (total) + price;
            total = total;
        });
        res.status(200).jsonp({"totalReal": total});

    } catch (e) {
        //res.send(500, e.message);
        next(e)
    }
}

exports.validPaymentProductOne = function (req, res, next) {
    var enc;
    var dec;
    var i;
    var status;
    var valueStatus = 0;
    try {
        enc = req.param('encode', null);
        dec = jwt.decode(enc, 'SECRET');
        i = dec.payload;
        Product.findOne({_id: i._id}).populate('categoryID')
            .populate('shopID').populate('taxID')
            .populate('coinsID').exec(function (err, j) {

            if (err) {
                res.send(500, err.message);
            }

            var amountPrices = j.amount.price.toFixed(2);
            var codprice = i.amount.price.toFixed(2);
            status = {"name": "pendiente", "type": valueStatus};

            if (codprice != amountPrices) {
                res.status(500).send({"message": "cambios en precios"});
            } else if (i.cant == 0) {
                res.status(500).send({"message": "Debe pedir al menos un producto"});
            } else if (i.cant > j.available) {
                res.status(500).send({"message": "no puede ser mayor a la solicitada"});
            } else if (j.available <= j.stock.min || j.stock.min == i.cant) {
                res.status(500).send({"message": "Agotado"});
            } else {
                res.status(200).send({message: "available", "payload": i});
            }

        });


    } catch (e) {
        //res.send(500, e.message);
        next(e)
    }
}

function saleProduct(id, payload) {
    Product.update({_id: id}, {$set: payload}, function (err, usr) {
        if (err) throw err;

    });
}
