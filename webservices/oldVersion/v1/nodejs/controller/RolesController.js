var Roles = require('../model/Roles');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

exports.RolesCRUD = function (prm, id, req, res, next) {

    var name;
    var readwrite;

    var payload;

    var t = middle.middlewareRolWSBackend("admin", req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Roles.find(function (err, Roless) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Roles');
                return res.status(200).jsonp(Roless);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = req.param('name', null);
            readwrite = req.param('readwrite', null);

            payload = {name: name, readwrite: readwrite};
            Roles.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Roles(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Roles.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            name = req.param('name', null);
            readwrite = req.param('readwrite', null);

            payload = {name: name, readwrite: readwrite};
            Roles.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Roles.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }
    });

}
