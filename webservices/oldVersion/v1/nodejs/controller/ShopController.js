var Model = require('../model/Shop');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

exports.shopCRUD = function (rol,prm, id, req, res, next) {

    var name;
    var description;
    var location;
    var geolocation;
    var payload;
    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Model.find(function (err, categorys) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Shop');
                return res.status(200).jsonp(categorys);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = req.param('name', null);
            description = req.param('description', null);
            location = req.param('location', null);
            geolocation = req.param('geolocation', null);

            payload = {name: name, description: description, location: location, geolocation: geolocation};
            Model.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Model(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Model.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            name = req.param('name', null);
            description = req.param('description', null);
            location = req.param('location', null);
            geolocation = req.param('geolocation', null);

            payload = {name: name, description: description, location: location, geolocation: geolocation};
            Model.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Model.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}
