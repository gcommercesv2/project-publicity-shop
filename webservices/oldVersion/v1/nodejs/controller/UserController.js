var User = require('../model/User');
var Roles = require('../model/Roles');
var Avatar = require('../model/Avatar');
var bcrypt = require('bcrypt');
var moment = require('moment');
var jwt = require('jwt-simple');
var textBody = require("body");
var crud = require('../utility/utility');
var middleware = require('../middleware/middleware');
var middle = require('../middleware/middleware');
const saltRounds = 10;

exports.register = function (req, res) {
    var name = req.param('name', null);
    var email = req.param('email', null);
    var password = req.param('password', null);
    var admin = false;
    var rolname = "user";
    var cruds = [{name: rolname, readwrite: [crud.arr_curd()[0]]}];
    var payload;

    Roles.findOne({name: rolname}, function (err, rock) {
        if (err) return next(err);

        var array = [rock];
        User.findOne({email: email}, function (err, usr) {

            if (usr == null || usr == undefined) {
                bcrypt.genSalt(saltRounds, function (err, salt) {
                    if (err) return next(err);

                    bcrypt.hash(password, salt, function (err, hash) {
                        if (err) return next(err);
                        password = hash;
                        payload = {
                            name: name,
                            email: email,
                            password: password,
                            admin: admin,
                            rolID: array,
                            roles: cruds
                        };
                        var usr = new User(payload);
                        usr.save(function (err, User) {
                            if (err) return res.send(500, err.message);
                            res.status(200).jsonp(User);
                        });

                    });
                });
            } else
                res.send({"message": "this recorset isset...!!!"});

        });
    });


};

exports.auth = function (req, res, next) {
    try {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado...!!!"});
        }

        var email = payload.email;
        User.findOne({email: email}, function (err, usr) {
            if (usr == null || usr == undefined) {
                return next();
            } else {
                res.send({name: usr.name});
            }
        });

    } catch (err) {
        return next();
    }
};


exports.setup = function (req, res) {
    var name = req.param('name', null);
    var email = req.param('email', null);
    var password = req.param('password', null);
    var admin = true;
    var rolname = "admin";
    var cruds = [{name: rolname, readwrite: crud.arr_curd()}];
    var payload;

    Roles.findOne({name: rolname}, function (err, rock) {
        if (err) return next(err);
        var array = [rock];
        User.findOne({email: email}, function (err, usr) {

            if (usr == null || usr == undefined) {
                bcrypt.genSalt(saltRounds, function (err, salt) {
                    if (err) return next(err);

                    bcrypt.hash(password, salt, function (err, hash) {
                        if (err) return next(err);
                        password = hash;
                        payload = {
                            name: name,
                            email: email,
                            password: password,
                            admin: admin,
                            rolID: array,
                            roles: cruds
                        };
                        var usr = new User(payload);
                        usr.save(function (err, User) {
                            if (err) return res.send(500, err.message);
                            res.status(200).jsonp(User);
                        });

                    });
                });
            } else
                res.send({message: "this recorset isset...!!!"});

        });
    });


};

exports.login = function (req, res, next) {

    var email;
    var password;
    var remember;
    var payload;
    try {
        email = req.param('email', null);
        password = req.param('password', null);
        remember = req.param('remember', null);
    } catch (err) {

    }

    try {
        User.findOne({email: email}, function (err, usr) {
            console.log("usr...." + usr);
            bcrypt.compare(password, usr.password, function (err, isMatch) {
                if (err) return (err);
                if (!isMatch) {
                    console.log("failed.....");
                    return res.sendStatus(401);
                }

                payload = {
                    'username': usr.name,
                    'email': usr.email,
                    'exp': moment().add(14, "days").unix()
                };
                var token = jwt.encode(payload, 'SECRET');

                res.status(200).send({message: "your logged...!", name: usr.name, email: usr.email, token: token});


            });
        });
    } catch (err) {
        next(err.message);
    }
};

exports.logout = function (req, res, next) {
    res.status(200).send({message: "your not logged...!", name: null, email: null, token: null});
}

exports.adminLogin = function (req, res) {
    var email = req.param('email', null);
    var password = req.param('password', null);
    var payload;
    User.findOne({email: email}, function (err, usr) {
        bcrypt.compare(password, usr.password, function (err, isMatch) {
            if (err) return (err);
            if (!isMatch) {
                console.log("failed");
                return res.send(401);
            }

            if (usr.admin == true) {
                payload = {
                    'username': usr.name,
                    'email': usr.email,
                    'admin': usr.admin,
                    'exp': moment().add(14, "days").unix()
                };
                var token = jwt.encode(payload, 'SECRET');

                res.status(200).send({message: "your logged...!", token: token});
            } else {
                res.status(401).send({message: "you are not admin...!"});
            }


        });
    });
};

exports.listuser = function (req, res, next) {
    User.find(function (err, users) {
        if (err) {
            res.send(500, err.message);
        }
        console.log('GET /Users')
        return res.status(200).jsonp(users);
    });
};

exports.infouser = function (req, res, next) {
    if (!req.headers.authorization) {
        return res
            .status(403)
            .send({message: "Tu petición no tiene cabecera de autorización"});
    }

    var token = req.headers.authorization.split(" ")[1];
    var payload = jwt.decode(token, 'SECRET');

    try {
        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado"});
        }

        req.user = payload.email;
        User.findOne({email: req.user}).populate('rolID').exec(function (err, usr) {

            if (usr == null || usr == undefined) {
                return res
                    .status(403)
                    .send({message: "Incorrect credentials"});
            } else {
                res.status(200).jsonp(usr);
            }
        });

    } catch (err) {
        next();
    }
};

exports.infoAvatar = function (req, res, next) {
    if (!req.headers.authorization) {
        return res
            .status(403)
            .send({message: "Tu petición no tiene cabecera de autorización"});
    }

    var token = req.headers.authorization.split(" ")[1];
    var payload = jwt.decode(token, 'SECRET');

    try {
        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado"});
        }

        req.user = payload.email;
        User.findOne({email: req.user}).populate('rolID').exec(function (err, usr) {

            //if (usr == null || usr == undefined) {
            //    return res
            //        .status(403)
            //        .send({message: "Incorrect credentials"});
            //} else {
            //    res.status(200).jsonp(usr);
            //}
            if (err) {
                res.send(500, err.message);
            }
            console.log("--- "+usr._id);
            Avatar.findOne({userID: usr._id}).populate('userID').exec(function (err, avt) {
                if (err) {
                    res.send(500, err.message);
                }
                res.status(200).jsonp(avt);
            });

        });

    } catch (err) {
        next();
    }
};

exports.userCRUD = function (prm, id, req, res, next) {

    var name;
    var email;
    var password;
    var admin;
    var roles;
    var rolID;
    var cruds;
    var payload;
    var isResetPassword;

    var t = middle.middlewareRolWSBackend("admin", req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {
            User.find().populate('rolID').exec(function (err, users) {
                if (err) {
                    res.send(500, err.message);
                }
                // console.log('GET /' + users)
                return res.status(200).jsonp(users);
            });
        } else if (prm == crud.vectorSave(vektor)) {
            console.log("body...:: " + JSON.stringify(req.body));
            name = req.param('name', null);
            email = req.param('email', null);
            password = req.param('password', null);
            admin = false;
            roles = req.param('roles', null);
            rolID = req.param('rolID', null);
            cruds = roles;
            User.findOne({email: email}, function (err, usr) {

                if (usr == null || usr == undefined) {
                    bcrypt.genSalt(saltRounds, function (err, salt) {
                        if (err) return next(err);

                        bcrypt.hash(password, salt, function (err, hash) {
                            if (err) return next(err);
                            password = hash;
                            payload = {
                                name: name,
                                email: email,
                                password: password,
                                admin: admin,
                                rolID: rolID,
                                roles: cruds
                            };
                            var usr = new User(payload);
                            usr.save(function (err, User) {
                                if (err) return res.send(500, err.message);
                                res.status(200).jsonp(User);
                            });

                        });
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });

        } else if (prm == crud.vectorEdit(vektor)) {
            User.findOne({_id: id}).populate('rolID').exec(function (err, usr) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(usr);
            });
        } else if (prm == crud.vectorUpdate(vektor)) {
            //-----------------------------------------------------------//
            console.log("body...:: " + JSON.stringify(req.body));
            name = req.param('name', null);
            email = req.param('email', null);
            password = req.param('password', null);
            admin = false;
            roles = req.param('roles', null);
            rolID = req.param('rolID', null);
            isResetPassword = req.param('isResetPassword', null);
            cruds = roles;
            //------------------------------------------------------------//
            if (isResetPassword == true) {
                bcrypt.genSalt(saltRounds, function (err, salt) {
                    if (err) return next(err);

                    bcrypt.hash(password, salt, function (err, hash) {
                        if (err) return next(err);
                        password = hash;
                        payload = {
                            name: name,
                            email: email,
                            password: password,
                            admin: admin,
                            rolID: rolID,
                            roles: cruds
                        };

                        updateUser(id, payload, res);
                    });
                });
            } else {
                payload = {
                    name: name,
                    email: email,
                    admin: admin,
                    rolID: rolID,
                    roles: cruds
                };
                updateUser(id, payload, res);
            }


            //------------------------------------------------------------//
        } else if (prm == crud.vectorRemove(vektor)) {
            User.remove({_id: id}, function (err, usr) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(usr);
            });
        } else {
            res.send({message: "you are not access this module...!!!"});
        }
    });
}

exports.resetPassword = function (req, res, next) {
    var email;
    var password;
    email = req.param('email', null);
    password = req.param('password', null);

    bcrypt.genSalt(saltRounds, function (err, salt) {
        if (err) return next(err);

        bcrypt.hash(password, salt, function (err, hash) {
            if (err) return next(err);
            password = hash;
            payload = {
                password: password,
            };

            User.updateOne({email: email}, {$set: payload}, function (err, usr) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(usr);
            });
        });
    });
}

function updateUser(id, payload, res) {
    User.updateOne({_id: id}, {$set: payload}, function (err, usr) {
        if (err) return res.send(500, err.message);
        res.status(200).jsonp(usr);
    });
}