var cors = require('cors');
var jwt = require('jwt-simple');
var bodyParser = require('body-parser');
var methodOverride = require("method-override");
var moment = require('moment');
//var mongoose = require('mongoose');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var mongoose = Promise.promisifyAll(mongoose);


//var moment = require('moment');
//var bcrypt = require('bcrypt');
//var textBody = require("body");
//var multer = require('multer');
var middle = require('./middleware/middleware');
var userctrl = require('./controller/UserController');
var productctrl = require('./controller/ProductController');
var shopctrl = require('./controller/ShopController');
var categoryctrl = require('./controller/CategoryController');
var rolesctrl = require('./controller/RolesController');
var apiurlctrl = require('./controller/ApiUrlController');
var mailctrl = require('./controller/MailController');
var avatarctrl = require('./controller/AvatarController');
var taxctrl = require('./controller/TaxController');
var coinsctrl = require('./controller/CoinsController');
var statusctrl = require('./controller/StatusController');
var chiperctrl = require('./controller/EncryptController');
// var Product = require('./model/Product');

var fs = require('fs'),
//https = require('https'),
    express = require('express'),
    app = express();
/*
 https.createServer({
 key: fs.readFileSync('key.pem'),
 cert: fs.readFileSync('cert.pem')
 }, app).listen(8100);*/
app.listen(8070);

mongoose.connect('mongodb://localhost:27017/dbpublicshop', function (err, res) {
    if (err) throw err;
    console.log('Connected to Database');
});

app.use(cors({
    'origin': '*'
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({keepExtensions: true, extended: true, uploadDir: './upload'}));
app.use(methodOverride());
app.use(express.static(__dirname));


var multipart = require('connect-multiparty');
app.use(multipart());

app.get('/', function (req, res) {
    res.header('Content-type', 'text/html');
    return res.end('<h1>Hello, Secure World!</h1>');
});
//--------------url administrator system-----------------------------------------------//
app.post('/api/register/', userctrl.register);

app.post('/api/setup/', userctrl.setup);

app.post('/api/login/', userctrl.login);

app.post('/api/admin/login/', userctrl.adminLogin);

app.post('/api/resetPassword/', middle.ensureAuthenticated, function (req, res, next) {
    userctrl.resetPassword(req, res, next);
});

app.post('/api/sendMailTo', function (req, res) {
    mailctrl.sendEmailTo(req, res);
});

app.post('/api/sendEmailNoReply', function (req, res) {
    mailctrl.sendEmailNoReply(req, res);
});

app.post('/api/sendMailMulti', function (req, res) {
    mailctrl.sendEmailMulti(req, res);
});

//this test send email
app.get('/api/sendMail', function (req, res) {
    mailctrl.sendEmail(req, res);
});

app.get('/api/auth/', userctrl.auth);

app.get('/api/list-user/', middle.middlewareAdmin, function (req, res) {
    userctrl.listuser(req, res);
});

app.get('/api/middleware/:rol/', function (req, res, next) {
    var role;
    try {
        role = req.params.rol
    } catch (err) {
        role = null;
    }
    middle.middlewareRolFrontend(role, req, res, next);
});

app.use('/api/user/:prm/:id/', middle.middlewareAdmin, function (req, res, next) {
    userctrl.userCRUD(req.params.prm, req.params.id, req, res, next);
});

app.use('/api/product/:prm/:id/', middle.middlewareRolBackend("product"), function (req, res, next) {
    productctrl.productCRUD("product",req.params.prm, req.params.id, req, res, next);
});

app.use('/api/category/:prm/:id/', middle.middlewareRolBackend("category"), function (req, res, next) {
    categoryctrl.categoryCRUD("category",req.params.prm, req.params.id, req, res, next);
});

//app.use('/api/shop/:prm/:id/', middle.middlewareRolBackendArray(["shop","category"]), function (req, res, next) {
app.use('/api/shop/:prm/:id/', middle.middlewareRolBackend("shop"), function (req, res, next) {
    shopctrl.shopCRUD("shop",req.params.prm, req.params.id, req, res, next);
});

app.use('/api/tax/:prm/:id/', middle.middlewareRolBackend("tax"), function (req, res, next) {
    taxctrl.taxCRUD("tax",req.params.prm, req.params.id, req, res, next);
});

app.use('/api/coins/:prm/:id/', middle.middlewareRolBackend("coins"), function (req, res, next) {
    coinsctrl.coinsCRUD("coins",req.params.prm, req.params.id, req, res, next);
});

app.use('/api/status/:prm/:id/', middle.middlewareRolBackend("status"), function (req, res, next) {
    statusctrl.statusCRUD("status",req.params.prm, req.params.id, req, res, next);
});

app.use('/api/roles/:prm/:id/', middle.middlewareRolBackend("admin"), function (req, res, next) {
    rolesctrl.RolesCRUD(req.params.prm, req.params.id, req, res, next);
});

app.use('/api/url/:prm/:id/', middle.middlewareRolBackend("admin"), function (req, res, next) {
    apiurlctrl.apiUrlCRUD(req.params.prm, req.params.id, req, res, next);
});

app.use('/api/avatar/:prm/:id/', middle.middlewareRolBackend("user"), function (req, res, next) {
    avatarctrl.avatarCRUD(req.params.prm, req.params.id, req, res, next);
});

app.use('/api/info/', middle.ensureAuthenticated, function (req, res, next) {
    userctrl.infouser(req, res, next);
});

app.use('/api/infoAvatar/', middle.ensureAuthenticated, function (req, res, next) {
    userctrl.infoAvatar(req, res, next);
});

app.use('/api/addAvatar', middle.ensureAuthenticated, function (req, res, next) {
    avatarctrl.addAvatar(req, res, next);
});

app.use('/api/logout/', middle.ensureAuthenticated, function (req, res, next) {
    userctrl.logout(req, res, next);
});

app.use('/api/productInfo/', middle.ensureAuthenticatedOauth, function (req, res, next) {
    productctrl.productInfo(req, res, next);
});
app.use('/api/productInfoDetail/:id', middle.ensureAuthenticatedOauth, function (req, res, next) {
    productctrl.productInfoDetail(req.params.id,req, res, next);
});

app.use('/api/chiper-payload/:prm',middle.ensureAuthenticatedOauth,function(req,res,next){
    chiperctrl.chiperData(req.params.prm,req,res,next);
});

app.use('/api/validPaymentProduct/', middle.ensureAuthenticatedOauth, function (req, res, next) {
    productctrl.validPaymentProduct(req, res, next);
});

app.use('/api/validPaymentProductOne/', middle.ensureAuthenticatedOauth, function (req, res, next) {
    productctrl.validPaymentProductOne(req, res, next);
});

//--------------------------------------------------------------------------------------////////////
console.log("Servidor Express escuchando en modo %s", app.settings.env);
