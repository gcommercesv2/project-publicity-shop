var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('ApiUrl', new Schema({
    name: String, 
    link: String
}));
