var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Shop', new Schema({
    name: String,
    description: String,
    location: String,
    geolocation: {lat: Number, long: Number}
}));