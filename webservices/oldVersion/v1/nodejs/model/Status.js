var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Status', new Schema({
    name: String,
    description: String
}));