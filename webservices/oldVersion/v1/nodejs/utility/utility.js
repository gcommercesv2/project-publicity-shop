const crud = ["read", "save", "edit", "update", "delete"];
var textBody = require("body");
var Promise = require('promise');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

exports.arr_curd = function () {
    return crud;
}

exports.requestData = function (req, res) {
    textBody(req, res, function (err, body) {
        //if(err) throw err;

        //return data;

        return new Promise(
            function (resolve, reject) {
                var data = JSON.parse(body);
                resolve(data);
                reject(error);
            });

    });
}


exports.vectorRead = function (v) {
    try {
        var read = crud[0];
        var indexRead = v.findIndex(function (item, i) {
            return item === read
        });

        var vt = v[indexRead];

        return vt;
    } catch (err) {
        return null;
    }

}

exports.vectorSave = function (v) {
    try {
        var save = crud[1];
        var indexSave = v.findIndex(function (item, i) {
            return item === save
        });

        var vt = v[indexSave];

        return vt;
    } catch (err) {
        return null;
    }

}

exports.vectorEdit = function (v) {
    try {
        var edit = crud[2];
        var indexEdit = v.findIndex(function (item, i) {
            return item === edit
        });

        var vt = v[indexEdit];

        return vt;
    } catch (err) {
        return null;
    }
}

exports.vectorUpdate = function (v) {
    try {
        var update = crud[3];
        var indexUpdate = v.findIndex(function (item, i) {
            return item === update
        });

        var vt = v[indexUpdate];

        return vt;
    } catch (err) {
        return null;
    }
}
exports.vectorRemove = function (v) {
    try {
        var remove = crud[4];
        var indexRemove = v.findIndex(function (item, i) {
            return item === remove
        });

        var vt = v[indexRemove];

        return vt;
    } catch (err) {
        return null;
    }
}

exports.encryptText = function (text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

exports.decryptText = function (text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

exports.encryptBuffer = function (buffer) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = Buffer.concat([cipher.update(buffer), cipher.final()]);
    return crypted;
}

exports.decryptBuffer = function (buffer) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = Buffer.concat([decipher.update(buffer), decipher.final()]);
    return dec;
}

exports.getIp = function (req) {
    var ip;
    if (req.headers['x-forwarded-for']) {
        ip = req.headers['x-forwarded-for'].split(",")[0];
    } else if (req.connection && req.connection.remoteAddress) {
        ip = req.connection.remoteAddress;
    } else {
        ip = req.ip;
    }
    //console.log("client IP is *********************" + ip);
    return ip;
}

exports.getHeadersSniffer = function (req, res, next) {
    var jsons;
    var str = JSON.stringify(req.headers);
    jsons = str;
    return jsons;
}