var ApiUrl = require('../model/ApiUrl');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

var name;
var link;
var payload;

exports.apiUrlCRUD = function (prm, id, req, res, next) {

    var t = middle.middlewareRolWSBackend("admin", req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            ApiUrl.find(function (err, Roless) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /ApiUrl');
                return res.status(200).jsonp(Roless);
            });

        } else if (prm == crud.vectorSave(vektor)) {
            name = reqName(req);
            payload = reqPayload(req);
            ApiUrl.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new ApiUrl(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            ApiUrl.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            //name = req.param('name', null);
            //link = req.param('link', null);
            //
            //payload = {name: name, link: link};
            payload = reqPayload(req);
            ApiUrl.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            ApiUrl.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }
    });
}
exports.setupUrlAllIsset = function (req, res, next) {
    try {
        var mod;
        mod = crud.arr_modules();
        mod.forEach(function (i) {
            console.log(".....name: " + i.name);
            name = i.name;
            payload = {
                name: i.name,
                link: i.link
            }
            ApiUrl.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    // var ct = new ApiUrl(payload);
                    // ct.save(function (err, p) {
                    //     if (err) return res.send(500, err.message);
                    //     // res.status(200).jsonp(p);
                    //     console.log("recorset: " + JSON.stringify(p));
                    // });
                    var ct = new ApiUrl();
                    ct.name = i.name;
                    ct.link = i.link;
                    ct.save();
                    // ct.save(function (err, p) {
                    //         if (err) return res.send(500, err.message);
                    //         // res.status(200).jsonp(p);
                    //         console.log("recorset: " + JSON.stringify(p));
                    //     });

                } else {
                    console.log("this recorset isset...!!!");
                }
                //res.send({message: "this recorset isset...!!!"});

            });
        });
        res.send({message: "success"});
    } catch (e) {
        next(e)
    }
}

exports.setupUrlAdmin = function (req, res, next) {
    //name = reqName(req);
    //payload = reqPayload(req);
    name = 'admin';
    payload = {name: 'admin', link: 'admin'};

    ApiUrl.findOne({name: name}, function (err, cat) {

        if (cat == null || cat == undefined) {

            var ct = new ApiUrl(payload);
            ct.save(function (err, p) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(p);
            });
        } else
            res.send({message: "this recorset isset...!!!"});

    });
}

function reqName(req) {
    name = req.param('name', null);
    return name;
}

function reqPayload(req) {
    name = req.param('name', null);
    link = req.param('link', null);
    payload = {name: name, link: link};
    return payload
}