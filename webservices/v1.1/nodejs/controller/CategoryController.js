var Category = require('../model/Category');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

var name;
var description;
var payload;
var process;
var module="Category";

exports.categoryCRUD = function (rol,prm, id, req, res, next) {
 
    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Category.findAsync(function (err, categorys) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Category');
                saveLog(prm,categorys,req,res,next);
                return res.status(200).jsonp(categorys);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = reqName(req);
            payload = reqPayload(req);

            Category.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Category(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        saveLog(prm,payload,req,res,next);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Category.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,cat,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            payload = reqPayload(req);
            
            Category.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,payload,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Category.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,id,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}

function saveLog(prm,payload,req,res,next){
    process ={module:module, status: prm, payload:payload};
    crud.saveLogSystem(process, req, res, next);
}

function reqName(req) {
    name = req.param('name', null);
    return name;
}

function reqPayload(req) {
    name = req.param('name', null);
    description = req.param('description', null);

    payload = {name: name, description: description};
            
    return payload
}