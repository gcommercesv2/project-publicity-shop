var Model = require('../model/Planner');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

var name;
var description;
var members;
var statusID;
var priority;
var endDatePlanner;
var userconn;
var userID;
var oauthID;
var payload;
var module="Planner";
var process;

exports.plannerCRUD = function (rol,prm, id, req, res, next) {

    
    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Model.find(function (err, categorys) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Shop');
                saveLog(prm,categorys,req,res,next);
                return res.status(200).jsonp(categorys);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = reqName(req);
            payload = reqPayload(req);

           Model.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Model(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        saveLog(prm,payload,req,res,next);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Model.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,cat,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {
            
            payload = reqPayload(req);

            Model.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,payload,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Model.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,id,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}

function saveLog(prm,payload,req,res,next){
    process ={module:module, status: prm, payload:payload};
    crud.saveLogSystem(process, req, res, next);
}

function reqName(req) {
    name = req.param('name', null);
    return name;
}

function reqPayload(req) {
    // userconn=crud.getUserConnect(req);
    // //userID=null;
    //  var id=userconn.then(function(x){
    //  console.log("-json---"+ JSON.stringify(x));
    // // userID=x;
    // return x;
    // });

    // userID=id.then(function(x){ return x;});

    // console.log("\n....id : "+id.then(function(x){ return x })); 
    userID=req.param('userID',null);
    name = req.param('name', null);
    description = req.param('description', null); 
    priority= req.param('priority',null);
    statusID=req.param('statusID',null);
    members=req.param('members',null);
    endDatePlanner= req.param('endDatePlanner',null);
    payload = {name: name, description: description,userID:userID,priority:priority,statusID,members:members,endDatePlanner:endDatePlanner};
            
    return payload;
    
}