var Roles = require('../model/Roles');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

var name;
var readwrite;
var payload;
var process;
var module = "Roles";
exports.RolesCRUD = function (prm, id, req, res, next) {

    var t = middle.middlewareRolWSBackend("admin", req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Roles.find(function (err, Roless) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Roles');
                // process ={module:module, status: prm, payload:Roless};
                // crud.saveLogSystem(process, req, res, next);
                saveLog(prm, Roless, req, res, next);

                return res.status(200).jsonp(Roless);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = reqName(req);
            payload = reqPayload(req);

            Roles.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Roles(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        // process ={module:module, status: prm, payload:payload};
                        // crud.saveLogSystem(process, req, res, next);
                        saveLog(prm, payload, req, res, next)
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Roles.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                // process ={module:module, status: prm, payload:cat};
                // crud.saveLogSystem(process, req, res, next);
                saveLog(prm, cat, req, res, next)
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            payload = reqPayload(req);

            Roles.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                // process ={module:module, status: prm ,payload:payload};
                // crud.saveLogSystem(process, req, res, next);
                saveLog(prm, payload, req, res, next)
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Roles.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                // process ={module:module, status: prm,payload:id };
                // crud.saveLogSystem(process, req, res, next);
                saveLog(prm, id, req, res, next)
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }
    });

}

exports.setupRolAdmin = function (req, res, next) {
    name = 'admin';
    readwrite = crud.arr_curd();

    payload = {name: name, readwrite: readwrite};
    Roles.findOne({name: name}, function (err, cat) {

        if (cat == null || cat == undefined) {

            var ct = new Roles(payload);
            ct.save(function (err, p) {
                if (err) return res.send(500, err.message);
                res.status(200).jsonp(p);
            });
        } else
            res.send({message: "this recorset isset...!!!"});

    });
}

function saveLog(prm, payload, req, res, next) {
    process = {module: module, status: prm, payload: payload};
    crud.saveLogSystem(process, req, res, next);
}

function reqName(req) {
    name = req.param('name', null);
    return name;
}

function reqPayload(req) {
    name = req.param('name', null);
    readwrite = req.param('readwrite', null);

    payload = {name: name, readwrite: readwrite};

    return payload
}