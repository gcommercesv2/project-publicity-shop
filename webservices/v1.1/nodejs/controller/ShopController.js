var Model = require('../model/Shop');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

var name;
var description;
var locationID;
var locationRef;
var payload;
var module="Shop";
var process;
exports.shopCRUD = function (rol,prm, id, req, res, next) {


    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Model.find().populate('locationID').exec(function (err, categorys) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Shop');
                saveLog(prm,categorys,req,res,next);
                return res.status(200).jsonp(categorys);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = reqName(req);
            payload = reqPayload(req);

            Model.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Model(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        saveLog(prm,payload,req,res,next);
                        res.status(200).jsonp(p);
                    });
                } else
                res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Model.findOne({_id: id}).populate('locationID').exec(function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,cat,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            payload = reqPayload(req);

            Model.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,payload,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Model.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                saveLog(prm,id,req,res,next);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}

exports.shopInfo=function(req,res,next){
    Model.find().populate('locationID').exec(function (err, categorys) {
        if (err) {
            res.send(500, err.message);
        }
                //console.log('GET /Shop');
        saveLog(prm,categorys,req,res,next);
        return res.status(200).jsonp(categorys);
    });
}

exports.shopInfoDetail=function(id,req,res,next){
    Model.find({_id:id}).populate('locationID').exec(function (err, categorys) {
        if (err) {
            res.send(500, err.message);
        }
                //console.log('GET /Shop');
        saveLog(prm,categorys,req,res,next);
        return res.status(200).jsonp(categorys);
    });
}

function saveLog(prm,payload,req,res,next){
    process ={module:module, status: prm, payload:payload};
    crud.saveLogSystem(process, req, res, next);
}

function reqName(req) {
    name = req.param('name', null);
    return name;
}

function reqPayload(req) {
    name = req.param('name', null);
    description = req.param('description', null);
    locationRef=req.param('locationRef',null);
    locationID = req.param('locationID', null);

    payload = {name: name, description: description,locationRef:locationRef, locationID: locationID};

    return payload
}