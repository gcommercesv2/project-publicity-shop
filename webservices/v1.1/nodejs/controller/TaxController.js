var Model = require('../model/Tax');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

var name;
var description;
var value;

var payload;
var process;
var module="Tax";

exports.taxCRUD = function (rol,prm, id, req, res, next) {

    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Model.find(function (err, categorys) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Shop');
                process ={module:module, status: prm, payload:categorys};
                crud.saveLogSystem(process, req, res, next);
                return res.status(200).jsonp(categorys);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = reqName(req);
            payload = reqPayload(req);

            Model.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Model(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        process ={module:module, status: prm, payload:payload};
                        crud.saveLogSystem(process, req, res, next);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Model.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                process ={module:module, status: prm, payload:cat};
                crud.saveLogSystem(process, req, res, next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {
            
            payload = reqPayload(req);

            Model.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                process ={module:module, status: prm,payload:payload};
                crud.saveLogSystem(process, req, res, next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Model.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                process ={module:module, status: prm,payload:id };
                crud.saveLogSystem(process, req, res, next);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}

function reqName(req) {
    name = req.param('name', null);
    return name;
}

function reqPayload(req) {
    name = req.param('name', null);
    description = req.param('description', null);
    value = req.param('value', null);

    payload = {name: name, description: description, value: value};

    return payload
}
