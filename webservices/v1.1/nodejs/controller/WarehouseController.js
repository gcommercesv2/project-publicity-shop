var Model = require('../model/Warehouse');

var crud = require('../utility/utility');
var middle = require('../middleware/middleware');

var name;
var description;
var position;
var payload;
var process;
var module = "Warehouse";

exports.wareHouseCRUD = function (rol, prm, id, req, res, next) {

    var t = middle.middlewareRolWSBackend(rol, req, res, next);
    var arrya;
    var vektor;

    t.then(function (x) {

        arrya = JSON.stringify(x);
        arrya = JSON.parse(arrya);
        vektor = arrya.readwrite;

        if (prm == crud.vectorRead(vektor)) {

            Model.findAsync(function (err, Models) {
                if (err) {
                    res.send(500, err.message);
                }
                //console.log('GET /Model');
                //process = module+", " +prm + " : " + Models;
                saveLog(prm, Models, req, res, next);
                return res.status(200).jsonp(Models);
            });

        } else if (prm == crud.vectorSave(vektor)) {

            name = reqName(req);
            payload = reqPayload(req);
            Model.findOne({name: name}, function (err, cat) {

                if (cat == null || cat == undefined) {

                    var ct = new Model(payload);
                    ct.save(function (err, p) {
                        if (err) return res.send(500, err.message);
                        //process = module+", "+prm + " : " + JSON.stringify(payload);
                        saveLog(prm, payload, req, res, next);
                        res.status(200).jsonp(p);
                    });
                } else
                    res.send({message: "this recorset isset...!!!"});

            });
        } else if (prm == crud.vectorEdit(vektor)) {

            Model.findOne({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                //process = module+", "+prm + " : " + cat;
                saveLog(prm, cat, req, res, next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorUpdate(vektor)) {

            payload = reqPayload(req);

            Model.updateOne({_id: id}, {$set: payload}, function (err, cat) {
                if (err) return res.send(500, err.message);
                //process = module+", "+prm + " : " + id +" "+JSON.stringify(payload);
                saveLog(prm, payload, req, res, next);
                res.status(200).jsonp(cat);
            });

        } else if (prm == crud.vectorRemove(vektor)) {

            Model.remove({_id: id}, function (err, cat) {
                if (err) return res.send(500, err.message);
                //process = module+", "+prm + " : " + id;
                saveLog(prm, id, req, res, next);
                res.status(200).jsonp(cat);
            });

        } else {
            res.send({message: "you are not access this module...!!!"});
        }

    });

}

function saveLog(prm, payload, req, res, next) {
    process = {module: module, status: prm, payload: payload};
    crud.saveLogSystem(process, req, res, next);
}

function reqName(req) {
    name = req.param('name', null);
    return name;
}

function reqPayload(req) {
    name = req.param('name', null);
    description = req.param('description', null);
    position = req.param('position', null);
    payload = {name: name,description:description,position:position};
    return payload
}