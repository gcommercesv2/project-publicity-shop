// var Promise = require('bluebird');
// var mongoose = require('mongoose');
// var mongoose = Promise.promisifyAll(mongoose);


var User = require('../model/User')
var Oauth = require('../oauth/model/Oauth')
//var Oauth = mongoose.model('Oauth')
//var bcrypt = require('bcrypt');
var moment = require('moment');
var jwt = require('jwt-simple');
var Q = require("q");

exports.ensureAuthenticated = function (req, res, next) {
    if (!req.headers.authorization) {
        return res
            .status(403)
            .send({message: "Tu petición no tiene cabecera de autorización"});
    }

    var token = req.headers.authorization.split(" ")[1];
    var payload = jwt.decode(token, 'SECRET');

    try {
        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado"});
        }

        req.user = payload.email;
        User.findOne({email: req.user}, function (err, usr) {

            if (usr == null || usr == undefined) {
                return res
                    .status(403)
                    .send({message: "Incorrect credentials"});
            } else {
                next();
            }
        });

    } catch (err) {
        next();
    }
};

exports.ensureAuthenticatedOauth = function (req, res, next) {
    if (!req.headers.authorization) {
        return res
            .status(403)
            .send({message: "Tu petición no tiene cabecera de autorización"});
    }

    var token = req.headers.authorization.split(" ")[1];
    var payload = jwt.decode(token, 'SECRET');

    try {
        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado"});
        }

        req.user = payload.email;
        Oauth.findOne({email: req.user}, function (err, usr) {

            if (usr == null || usr == undefined) {
                //return res
                //    .status(403)
                //    .send({message: "Incorrect credentials"});
                 User.findOne({email: req.user}, function (err, usrs) {
                    if (usrs == null || usrs == undefined) {
                        return res
                        .status(403).send({message: "Incorrect credentials"});
                    } else {
                        next();
                    }
                });
            } else {
                next();
            }
        });

       

    } catch (err) {
        next();
    }
};

exports.middlewareAdmin = function (req, res, next) {

    var roles;
    var val;
    var rol = "admin";
    var index;

    try {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado...!!!"});
        }

        var email = payload.email;

        User.findOne({email: email}).populate('rolID').exec(function (err, usr) {
            if (usr == null || usr == undefined) {
                return next();
            } else {
                roles = usr.rolID;
                val = rol;
                index = roles.findIndex(function (item, i) {
                    return item.name === val
                });
                if (usr.rolID[index] == null || usr.rolID[index] == undefined) {
                    res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                } else if (usr.rolID[index].name == rol) {
                    next();
                } else {
                    res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                }
            }
        });

    } catch (err) {
        return next();
    }

}

exports.middlewareRolFrontend = function (rol, req, res, next) {
    var roles;
    var val = rol;
    var index;

    try {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado...!!!"});
        }

        var email = payload.email;

        User.findOne({email: email}).populate('rolID').exec(function (err, usr) {
            if (usr == null || usr == undefined) {
                return next();
            } else {
                roles = usr.rolID;
                val = rol;
                index = roles.findIndex(function (item, i) {
                    return item.name === val
                });
                if (usr.rolID[index] == null || usr.rolID[index] == undefined) {
                    res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                } else if (usr.rolID[index].name == rol) {
                    res.send({name: usr.name, "enabled": rol, "readwrite": usr.rolID[index].readwrite});
                } else {
                    res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                }
            }
        });

    } catch (err) {
        return next();
    }

};

exports.middlewareRolBackend = function (rol) {
    var roles;
    var val = rol;
    var index;
    return function (req, res, next) {

        try {
            if (!req.headers.authorization) {
                return res
                    .status(403)
                    .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
            }

            var token = req.headers.authorization.split(" ")[1];
            var payload = jwt.decode(token, 'SECRET');

            if (payload.exp <= moment().unix()) {
                return res
                    .status(401)
                    .send({message: "El token ha expirado...!!!"});
            }

            var email = payload.email;

            User.findOne({email: email}).populate('rolID').exec(function (err, usr) {
                if (usr == null || usr == undefined) {
                    return next();
                } else {
                    roles = usr.rolID;
                    val = rol;
                    index = roles.findIndex(function (item, i) {
                        return item.name === val
                    });

                    if (usr.rolID[index] == null || usr.rolID[index] == undefined) {
                        res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    } else if (usr.rolID[index].name == rol) {
                        next();
                    } else {
                        res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    }
                }
            });

        } catch (err) {
            return next();
        }

    }
};

exports.middlewareRolWSBackend = function (rol, req, res, next) {
    return getRoles(rol, req, res, next);
};

function getRoles(rol, req, res, next) {
    var roles;
    var val = rol;
    var index;
    var deferred = Q.defer();

    try {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado...!!!"});
        }

        var email = payload.email;

        User.findOne({email: email}).populate('rolID').exec(function (err, usr) {
                //return new Promise(
                //  function (resolve, reject) {
                if (usr == null || usr == undefined) {
                    return next();
                } else {
                    roles = usr.rolID;
                    val = rol;
                    index = roles.findIndex(function (item, i) {
                        return item.name === val
                    });

                    if (usr.rolID[index] == null || usr.rolID[index] == undefined) {
                        res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    } else if (usr.rolID[index].name == rol) {

                        if (err) {
                            // Throw an error
                            deferred.reject(new Error(err));
                        }
                        else {
                            // No error, continue on
                            deferred.resolve({"readwrite": usr.rolID[index].readwrite});
                        }


                    } else {
                        res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    }
                }
            }
        );
        return deferred.promise;

    } catch (err) {
        return next();
    }
}

exports.middlewareRolFrontendArray = function (rol, req, res, next) {
    var roles;
    var val = rol;
    var index;

    try {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado...!!!"});
        }

        var email = payload.email;

        User.findOne({email: email}).populate('rolID').exec(function (err, usr) {
            if (usr == null || usr == undefined) {
                return next();
            } else {
                roles = usr.rolID;
                val = rol;
                for (var j = 0; j <= val.length; j++) {
                    index = roles.findIndex(function (item, i) {
                        return item.name === val[j]
                    });
                    if (usr.rolID[index] == null || usr.rolID[index] == undefined) {
                        res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    } else if (usr.rolID[index].name == rol) {
                        res.send({name: usr.name, "enabled": rol, "readwrite": usr.rolID[index].readwrite});
                    } else {
                        res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    }
                }
            }
        });

    } catch (err) {
        return next();
    }

};
//-------------------------//--------------in development---------------//--------------------------------------------//
exports.middlewareRolBackendArray = function (rol) {
    var roles;
    var val = rol;
    var index;
    return function (req, res, next) {

        try {
            if (!req.headers.authorization) {
                return res
                    .status(403)
                    .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
            }

            var token = req.headers.authorization.split(" ")[1];
            var payload = jwt.decode(token, 'SECRET');

            if (payload.exp <= moment().unix()) {
                return res
                    .status(401)
                    .send({message: "El token ha expirado...!!!"});
            }

            var email = payload.email;

            User.findOne({email: email}).populate('rolID').exec(function (err, usr) {
                if (usr == null || usr == undefined) {
                    return next();
                } else {
                    roles = usr.rolID;
                    val = rol;

                    //index[j] = roles.findIndex(function (item, i) {
                    //    return item.name === val[j]
                    //});
                    //console.log("index... "+index[j]);

                    //if (usr.rolID[index] == null || usr.rolID[index] == undefined) {
                    //    res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    //} else if (usr.rolID[index].name == rol[j]) {
                    //    console.log("rol... "+rol[j]);
                    //    next();
                    //} else {
                    //    res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                    //}

                    //roles.forEach(function (i, v) {
                        for (var j = 0; j <= val.length; j++) {
                            //index=j;
                            console.log("rol... " + rol[j]);
                            index[j] = roles.findIndex(function (item, i) {
                                return item.name === val[j]
                            });
                            console.log("roasl... " + index[j]);
                           // console.log("  v " + v + " nsame " + i.name);

                            if (usr.rolID[j] == null || usr.rolID[j] == undefined) {
                                res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                            } else if (usr.rolID[j].name == rol[j]) {
                                console.log("rol... " + rol[j]);
                                next();
                            } else {
                                res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                            }
                        }
                    //});

                }
            });

        } catch (err) {
            return next();
        }

    }
};

exports.middlewareRolWSBackendArray = function (rol, req, res, next) {
    return getRolesArray(rol, req, res, next);
};

function getRolesArray(rol, req, res, next) {
    var roles;
    var val = rol;
    var index;
    var deferred = Q.defer();

    try {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización...!!!"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado...!!!"});
        }

        var email = payload.email;

        User.findOne({email: email}).populate('rolID').exec(function (err, usr) {
                //return new Promise(
                //  function (resolve, reject) {
                if (usr == null || usr == undefined) {
                    return next();
                } else {
                    roles = usr.rolID;
                    val = rol;
                    for (var j = 0; j <= val.length; j++) {
                        index = roles.findIndex(function (item, i) {
                            return item.name === val[j]
                        });

                        if (usr.rolID[index] == null || usr.rolID[index] == undefined) {
                            res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                        } else if (usr.rolID[index].name == rol) {

                            if (err) {
                                // Throw an error
                                deferred.reject(new Error(err));
                            }
                            else {
                                // No error, continue on
                                deferred.resolve({"readwrite": usr.rolID[index].readwrite});
                            }


                        } else {
                            res.status(403).send({"enabled": 0, "message": "No Authorization this profile"});
                        }
                    }
                }
            }
        );
        return deferred.promise;

    } catch (err) {
        return next();
    }
}