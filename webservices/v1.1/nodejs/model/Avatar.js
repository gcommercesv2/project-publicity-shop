var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Avatar', new Schema({
    image: String,
    userID: {type: Schema.Types.ObjectId, ref: 'User'},
}));

