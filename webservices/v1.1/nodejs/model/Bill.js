var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Bill', new Schema({
	created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    paymentID: {type: Schema.Types.ObjectId, ref: 'Payment'},
    statusID: {type: Schema.Types.ObjectId, ref: 'Status'}
})); 
