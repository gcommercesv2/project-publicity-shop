var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Coins', new Schema({
    name: String,
    description: String,
    value: Number,
    locationID: {type: Schema.Types.ObjectId, ref: 'Location'},
}));