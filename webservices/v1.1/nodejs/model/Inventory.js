var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Inventory', new Schema({
	created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    statusID: {type: Schema.Types.ObjectId, ref: 'Status'},
    productID: {type: Schema.Types.ObjectId, ref: 'Product'}
}));