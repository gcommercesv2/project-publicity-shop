var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('LogSystem', new Schema({
    userID: {type: Schema.Types.ObjectId, ref: 'User'},
    oauthID: {type: Schema.Types.ObjectId, ref: 'Oauth'},
    process: {}, 
    ipAddress: String,
    sniffering:String,
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
}));
