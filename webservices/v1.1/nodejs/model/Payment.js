var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Payment', new Schema({
	created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    oauthID: {type: Schema.Types.ObjectId, ref: 'Oauth'},
    userID: {type: Schema.Types.ObjectId, ref: 'User'},
    statusID: {type: Schema.Types.ObjectId, ref: 'Status'},
    methodPaymentID: {type: Schema.Types.ObjectId, ref: 'MethodPayment'},
    providerID: {type: Schema.Types.ObjectId, ref: 'Provider'},
   	productID: {type: Schema.Types.ObjectId, ref: 'Product'}
}));
