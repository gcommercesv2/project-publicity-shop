var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Planner', new Schema({
    name: String,
    description: String,
    members: String,
    priority: String,
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    endDatePlanner: {type: Date, default: Date.now},
    statusID: {type: Schema.Types.ObjectId, ref: 'Status'},
    userID: {type: Schema.Types.ObjectId, ref: 'User'},
    oauthID: {type: Schema.Types.ObjectId, ref: 'Oauth'}   

}));