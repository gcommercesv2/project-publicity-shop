var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Product', new Schema({    
    available: Number,
    status: {name: String, value: Number},
    code: String,
    name: String,
    description: String,
    image: String,
    imagebase64: String,
    cost: Number,
    amount: {price: Number, special_price: Number},    
    date: {type: Date, default: Date.now},
    created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    stock: {min: Number, max: Number},
    statusID: {type: Schema.Types.ObjectId, ref: 'Status'},
    coinsID: {type: Schema.Types.ObjectId, ref: 'Coins'},
    typeDefID: {type: Schema.Types.ObjectId, ref: 'TypeDef'},
    warehouseID: {type: Schema.Types.ObjectId, ref: 'Warehouse'},
    categoryID: {type: Schema.Types.ObjectId, ref: 'Category'},
    taxID: {type: Schema.Types.ObjectId, ref: 'Tax'},
    shopID: {type: Schema.Types.ObjectId, ref: 'Shop'}   

}));