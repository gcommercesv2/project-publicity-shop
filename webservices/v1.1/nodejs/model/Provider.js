var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Provider', new Schema({
    name: String,
    typeOf: String,
    dni: String,
    address:String,
    other:String
}));