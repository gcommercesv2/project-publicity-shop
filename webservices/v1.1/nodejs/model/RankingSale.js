var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('RankingSale', new Schema({
	created_at: {type: Date, default: Date.now},
    updated_at: {type: Date, default: Date.now},
    quantity: Number,
    productID: {type: Schema.Types.ObjectId, ref: 'Product'},
    shopID: {type: Schema.Types.ObjectId, ref: 'Shop'}
}));