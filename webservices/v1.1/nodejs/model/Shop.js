var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Shop', new Schema({
    name: String,
    description: String,
    locationRef: String,
    locationID: {type: Schema.Types.ObjectId, ref: 'Location'},
}));