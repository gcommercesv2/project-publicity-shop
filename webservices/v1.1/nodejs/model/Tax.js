var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Tax', new Schema({
    name: String,
    description: String,
    value: Number
}));