var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('TypeDef', new Schema({
    name: String,
    value: Number 
}));

