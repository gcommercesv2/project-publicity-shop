var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('User', new Schema({
    avatar: String,
    name: String,
    email: String,
    password: String,
    admin: Boolean,
    rolID: [{type: Schema.Types.ObjectId, ref: 'Roles'}],
    avatarID: {type: Schema.Types.ObjectId, ref: 'Avatar'},
    roles: [{name: String, readwrite: []}]
}));