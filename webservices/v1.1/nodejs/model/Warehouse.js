var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Warehouse', new Schema({
    name: String,
    description: String,
    position:{x:String,y:String,z:String,other:String}
}));