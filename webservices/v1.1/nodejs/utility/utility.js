const crud = ["read", "save", "edit", "update", "delete"];
const modules = [
        {name: "admin", link: "admin"},
        {name: "user", link: "user"},
        {name: "shop", link: "shop"},
        {name: "product", link: "product"},
        {name: "status", link: "status"},
        {name: "bill", link: "bill"},
        {name: "tax", link: "tax"},
        {name: "category", link: "category"},
        {name: "coins", link: "coins"},
        {name: "location", link: "location"},
        {name: "inventory", link: "inventory"},
        {name: "provider", link: "provider"},
        {name: "warehouse", link: "warehouse"},
        {name: "payment", link: "payment"},
        {name: "planner", link: "planner"},
        {name: "sales", link: "sales"}
    ]
;
var activeLog = 1;
var logsystem = require('../model/LogSystem');
var User = require('../model/User');
var Oauth = require('../oauth/model/Oauth');
var textBody = require("body");
var jwt = require("jwt-simple");
var moment = require('moment');
var Promise = require('promise');
var Q = require("q");
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

exports.arr_curd = function () {
    return crud;
}

exports.arr_modules = function () {
    return modules;
}

exports.requestData = function (req, res) {
    textBody(req, res, function (err, body) {
        //if(err) throw err;

        //return data;

        return new Promise(
            function (resolve, reject) {
                var data = JSON.parse(body);
                resolve(data);
                reject(error);
            });

    });
}


exports.vectorRead = function (v) {
    try {
        var read = crud[0];
        var indexRead = v.findIndex(function (item, i) {
            return item === read
        });

        var vt = v[indexRead];

        return vt;
    } catch (err) {
        return null;
    }

}

exports.vectorSave = function (v) {
    try {
        var save = crud[1];
        var indexSave = v.findIndex(function (item, i) {
            return item === save
        });

        var vt = v[indexSave];

        return vt;
    } catch (err) {
        return null;
    }

}

exports.vectorEdit = function (v) {
    try {
        var edit = crud[2];
        var indexEdit = v.findIndex(function (item, i) {
            return item === edit
        });

        var vt = v[indexEdit];

        return vt;
    } catch (err) {
        return null;
    }
}

exports.vectorUpdate = function (v) {
    try {
        var update = crud[3];
        var indexUpdate = v.findIndex(function (item, i) {
            return item === update
        });

        var vt = v[indexUpdate];

        return vt;
    } catch (err) {
        return null;
    }
}
exports.vectorRemove = function (v) {
    try {
        var remove = crud[4];
        var indexRemove = v.findIndex(function (item, i) {
            return item === remove
        });

        var vt = v[indexRemove];

        return vt;
    } catch (err) {
        return null;
    }
}

exports.encryptText = function (text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

exports.decryptText = function (text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

exports.encryptBuffer = function (buffer) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = Buffer.concat([cipher.update(buffer), cipher.final()]);
    return crypted;
}

exports.decryptBuffer = function (buffer) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = Buffer.concat([decipher.update(buffer), decipher.final()]);
    return dec;
}

exports.saveLog = function (obj, req, res, next) {
    var module, prm, payload;
    module = obj.module;
    prm = obj.prm;
    payload = obj.payload;
    process = {module: module, status: prm, payload: payload};
    saveLogSystems(process, req, res, next);
}

exports.saveLogSystem = function (process, req, res, next) {

    if (activeLog != 0) {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        try {
            if (payload.exp <= moment().unix()) {
                return res
                    .status(401)
                    .send({message: "El token ha expirado"});
            }

            req.user = payload.email;
            Oauth.findOne({email: req.user}, function (err, usr) {

                if (usr == null || usr == undefined) {
                    //return res
                    //    .status(403)
                    //    .send({message: "Incorrect credentials"});
                    User.findOne({email: req.user}, function (err, usrs) {
                        if (usrs == null || usrs == undefined) {
                            return res
                                .status(403).send({message: "Incorrect credentials"});
                        } else {
                            //next();
                            console.log("is user: " + usrs._id + " ../: " + process);
                            fnLogSystem(usrs._id, "user", process, req);

                        }
                    });
                } else {
                    //next();
                    console.log("is oauth: " + usr._id);
                    fnLogSystem(usr._id, "oauth", process, req);
                }

            });


        } catch (err) {
            console.log(" error: " + err);
            // next();
        }
    }
}

exports.getUserConnect=function(req){
    var deferred = Q.defer();
    var fnusr;
    if (!req.headers.authorization) {
            console.log(""+{message: "Tu petición no tiene cabecera de autorización"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        try {
            if (payload.exp <= moment().unix()) {
                console.log(""+{message: "El token ha expirado"});
            }

            req.user = payload.email;
            Oauth.findOne({email: req.user}, function (err, usr) {
                deferred.reject(new Error(err));
                if (usr == null || usr == undefined) {
                    //return res
                    //    .status(403)
                    //    .send({message: "Incorrect credentials"});
                    User.findOne({email: req.user}, function (err, usrs) {
                        deferred.reject(new Error(err));
                        if (usrs == null || usrs == undefined) {
                            console.log(""+{message: "Incorrect credentials"});
                        } else {
                            //next();
                           // console.log("is user: " + usrs._id + " ../: " + process);
                            //fnUserConnect(usrs._id, "user");
                            //fnusr=fnUserConnect(usrs._id, "user");
                            deferred.resolve({userID:usr._id});
                            //deferred.resolve(fnusr);

                        }
                    });
                } else {
                    //next();
                    //console.log("is oauth: " + usr._id);   
                    //fnUserConnect(usr._id, "oauth");
                    //fnusr=fnUserConnect(usr._id, "user");
                    deferred.resolve({userID:usr._id});
                    //deferred.resolve(fnusr);
                }

            });

            return deferred.promise;

        } catch (err) {
            
            console.log(" error: " + err);
            // next();
        }
}

exports.saveLogSystemLogin = function (user, type, process, req, res, next) {

    if (activeLog != 0) {
        fnLogSystem(user, type, process, req);
    }
}


exports.getIp = function (req) {
    var ip;
    if (req.headers['x-forwarded-for']) {
        ip = req.headers['x-forwarded-for'].split(",")[0];
    } else if (req.connection && req.connection.remoteAddress) {
        ip = req.connection.remoteAddress;
    } else {
        ip = req.ip;
    }
    //console.log("client IP is *********************" + ip);
    return ip;
}

function saveLogSystems(process, req, res, next) {

    if (activeLog != 0) {
        if (!req.headers.authorization) {
            return res
                .status(403)
                .send({message: "Tu petición no tiene cabecera de autorización"});
        }

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, 'SECRET');

        try {
            if (payload.exp <= moment().unix()) {
                return res
                    .status(401)
                    .send({message: "El token ha expirado"});
            }

            req.user = payload.email;
            Oauth.findOne({email: req.user}, function (err, usr) {

                if (usr == null || usr == undefined) {
                    //return res
                    //    .status(403)
                    //    .send({message: "Incorrect credentials"});
                    User.findOne({email: req.user}, function (err, usrs) {
                        if (usrs == null || usrs == undefined) {
                            return res
                                .status(403).send({message: "Incorrect credentials"});
                        } else {
                            //next();
                            console.log("is user: " + usrs._id + " ../: " + process);
                            fnLogSystem(usrs._id, "user", process, req);

                        }
                    });
                } else {
                    //next();
                    console.log("is oauth: " + usr._id);
                    fnLogSystem(usr._id, "oauth", process, req);
                }

            });


        } catch (err) {
            console.log(" error: " + err);
            // next();
        }
    }
}

exports.getHeadersSniffer = function (req, res, next) {
    var jsons;
    var str = JSON.stringify(req.headers);
    jsons = str;
    return jsons;
}

function fnUserConnect(auth, isOauth) {
     var user;
    //if(my_ip!=local){
    if (isOauth == "oauth") {
        user = auth;
    } else {
        user = auth;
    }
    console.log("\n"+"user....: "+user);
    return user;
}

function fnLogSystem(auth, isOauth, procces, req) {
    var my_ip = getIps(req);
    var local = "::1";
    console.log("mi ip: " + my_ip);
    var ls = new logsystem();

    //if(my_ip!=local){
    if (isOauth == "oauth") {
        ls.oauthID = auth;
    } else {
        ls.userID = auth;
    }

    ls.process = procces;
    ls.sniffering = getHeadersSniffers(req);
    ls.ipAddress = getIps(req);
    ls.save();
    // }
}

function getIps(req) {
    var ip;
    if (req.headers['x-forwarded-for']) {
        ip = req.headers['x-forwarded-for'].split(",")[0];
    } else if (req.connection && req.connection.remoteAddress) {
        ip = req.connection.remoteAddress;
    } else {
        ip = req.ip;
    }
    //console.log("client IP is *********************" + ip);
    return ip;
}

function getHeadersSniffers(req) {
    var jsons;
    var str = JSON.stringify(req.headers);
    jsons = str;
    return jsons;
}