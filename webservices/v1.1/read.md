mongodump --archive=/home/ander/project-publicity-shop/database/dbpublicshop.20170907.archive --db dbpublicshop
mongorestore --archive=dbpublicshop.20170824.archive --db dbpublicshop

Generate the certificate

The certificate files are stored inside the ssl/ folder. You can generate new files with the following commands.

Create the private key :

openssl genrsa -out key.pem 1024
Create the "Certificate Signing Request" :

openssl req -new -key key.pem -out csr.pem
Create the self-signed certificate :

openssl x509 -req -in csr.pem -signkey key.pem -out cert.pem

//////////////

openssl req -newkey rsa:2048 -new -nodes -keyout key.pem -out csr.pem
openssl x509 -req -days 365 -in csr.pem -signkey key.pem -out server.crt
then use the server.crt

   var options = {
      key: fs.readFileSync('./key.pem', 'utf8'),
      cert: fs.readFileSync('./server.crt', 'utf8')
   };